
<?php get_header(); ?>



<?php
if (get_field('align4', 'options')=="left"){
    $align="left";
}
else{
    $align="right";
}
?>
<header class="secondary-header  single-reference ref-bg"   style="background: url(<?php the_field('ref_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
    <div class="header-shaddow">

        <div class="row content">
            <?php get_template_part('parts/category', 'menu') ?>
        </div>

        <div class="row content">
            <h1>Reference</h1>
        </div>

    </div>
</header>






<div class="container subpage reduce-pad-bot reference-page">
    <?php



    $terms = get_terms( 'platforms', array(
        'orderby'    => 'count',
        'hide_empty' => 0
    ) );



    echo '<div class="filter-wrap filter-wrap-platform">';
    echo '<span class="filter filter-title">Platforma:</span>';


    $i = 0;
    $len = count($terms);
    foreach ($terms as $term) {

        $current_term = is_tax ? get_queried_object() : null;
        $classactive = "";
        if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
            $classactive = "active-filter";
        }


        if ($i == $len - 1) {
            echo '<span class="filter filter-main filter-main-last ' . $classactive . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
        }
        else {
            echo '<span class="filter filter-main ' . $classactive . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
        }
        $i++;
    }


    echo '</div>';
    ?>

</div>









<div class="container-larger reduce-pad-bot subpage reference-page">
    <div class="row child-ref">

        <?php
        $queried_object = get_queried_object();
        $current_slug = $queried_object->slug;
        $paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
        $pageItems = $_GET["query"] ? $_GET["query"] : 9; //9 * $paged;
        $args = array(
                'post_type' => 'reference',
                'posts_per_page' => $pageItems,
                'paged' => $paged,
                'tax_query' => array(
                    [
                        'taxonomy' => 'platforms',
                        'field' => 'slug',
                        'terms' => $current_slug,
                    ]
    ),);
        $posts = query_posts($args);
        //echo $current_slug;
        ?>
        <?php  if ( have_posts()) while ( have_posts() ) : the_post(); ?>


            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                <a href="<?php the_permalink()?>">


                    <div class="ref-card">





                        <h2><?php the_field('nadpis_reference') ?></h2>

                        <span class="ref-what">

        <?php
        $terms = wp_get_post_terms( $post->ID,'platforms', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {


            if ($len > 1){
                if ($len > 2){
                    if ($i == 0) {
                        echo $term->name . ', ';
                    }
                    else if ($i == $len - 1) {
                        echo ' a ' . $term->name;
                    }

                    else {
                        echo $term->name;
                    }
                    $i++;
                }
                else {
                    if ($i == $len - 1) {
                        echo ' a ' . $term->name;
                    }
                    else {
                        echo $term->name;
                    }
                    $i++;

                }
            }



            else {
                echo $term->name;
            }

        }

        ?>

                            appka
                        </span>
                        <div class="ref-inclusion-wrap">


                            <?php
                            $terms = wp_get_post_terms( $post->ID,'type', array(
                                'orderby'    => 'count',
                                'hide_empty' => 1
                            ) );



                            foreach ($terms as $term) {


                                echo '<span class="ref-inclusion">' . $term->name . '</span>';

                            }



                            ?>

                        </div>

                        <?php if(get_field('desktop_mobile_video')=="mobil"){ ?>
                            <img class="ref-img" src="<?php the_field('ilustracni_fotografie') ?>">
                        <?php } ?>

                        <?php if(get_field('desktop_mobile_video')=="desktop"){ ?>
                            <img class="ref-img ref-img--desktop" src="<?php the_field('ilustracni_fotografie') ?>">
                        <?php } ?>
                    </div>
                </a>
            </div>

        <?php endwhile; ?>



        <?php if( $paged >= 1 ): ?>
            <div class="ref-button-wrap col-xs-12">
                <!-- <?php previous_posts_link('<button class="ref-button">Předchozí reference</button>'); ?>

                <?php next_posts_link('<button class="ref-button">Další reference</button>', $loop->max_num_pages); ?> -->
                
                <?php 
                    // $platform_args = array( 
                    //     'post_type' => 'reference',
                    //      'tax_query' => array(
                    //     [
                    //         'taxonomy' => 'platforms',
                    //         'field' => 'slug',
                    //         'terms' => $current_slug,
                    //     ]));
                    // $platform_query = new WP_Query( $platform_args );
                    // $total_platform_posts = $platform_query->found_posts;
                    // if(intval($total_platform_posts) >= intval($_GET["query"]) || !$_GET["query"] || intval($total_platform_posts) != 9){
                    //     echo '<button class="ref-button" id="more_refs_platform">Další reference</button>';
                    //     echo gettype(intval($total_platform_posts));
                    //     echo gettype(intval($_GET["query"]));
                    // }else{
                    //     echo "";
                    // }
                    // ?>
                        <?php 
                    $argss = array(
                        'post_type' => 'reference',
                        'tax_query' => array(
                            ['taxonomy' => 'platforms',
                            'field' => 'slug',
                            'terms' => $current_slug]
                        )
                    );

                    $query = new WP_Query( $argss );
                    $total = $query->found_posts;

                    if($total >= intval($_GET["query"]) || !$_GET["query"]){
                        echo '<button class="ref-button" id="more_refs_platform">Další reference</button>';
                    }else{
                        echo "";
                    }
                    ?>

                
            </div>
        <?php  endif; ?>
    </div>
</div>



<?php wp_reset_postdata() ?>
<?php wp_reset_query() ?>





<div class="what-next__wrap-other">
    <?php get_template_part('parts/category', 'what-next') ?>
</div>




<footer class="secondary-footer">
    <?php get_template_part('parts/category', 'short-contact') ?>
</footer>



<?php get_footer(); ?>