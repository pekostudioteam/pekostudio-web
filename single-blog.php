<?php include "header.php";?>
<!-- Hlavička - začátek -->


<?php
if (get_field('align2', 'options')=="left"){
    $align="left";
}
else{
    $align="right";
}
?>
<header class="secondary-header blog-bg"    style="background: url(<?php the_field('blog_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
    <div class="header-shaddow">

        <div class="row content">
            <?php include "parts/category-menu.php"; ?>
        </div>

        <div class="row content">
            <h1>Blog</h1>
        </div>

    </div>
</header>



<div class="container blog-detail-page">
    <div class="row">

        <div class="blog-detail-page--gray"></div>

        <div class="col-md-12 detail-blog-h2-phone">
            <h2 class="detail-blog--h2">Jak urychlit programování v PHP?<br>10 užitečných tipů</h2>
        </div>


        <div class="col-xs-4 detail-blog--date date__phone">
            <p>2.3.2018</p>
        </div>

        <div class="col-xs-8 detail-blog--signature signature__phone">
            <p>Petr Kott</p>
        </div>


        <div class="col-lg-5 col-md-5 col-sm-5">
            <img class="detail-blog--photo" alt="jak-urychlit-programovani-v-PHP-photo" src="images/blog-foto3.png">
        </div>

        <div class="col-lg-7 col-md-7 col-sm-7">
            <div class="row">
                <div class="col-lg-12 detail-blog-h2-desktop">
                    <h2 class="detail-blog--h2">Jak urychlit programování v PHP?<br>10 užitečných tipů</h2>
                </div>

                <div class="col-xs-4 detail-blog--date date__desktop">
                    <p>2.3.2018</p>
                </div>

                <div class="col-xs-8 detail-blog--signature signature__desktop">
                    <p>Petr Kott</p>
                </div>
                <div class="col-xs-12 detail-blog--p-wrap">
                    <p class="detail-blog--p">Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum.</p>
                </div>
            </div>
        </div>




        <div class="col-xs-12 blog-detail-content">
            <p>
                Aenean placerat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Vivamus ac leo <b>pretium</b> faucibus. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.
            </p>
            <p>
                Aenean placerat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat <strong>nulla</strong> pariatur. Vivamus ac leo pretium faucibus. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
            </p>
            <p>
                Aenean placerat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Vivamus ac leo pretium faucibus. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Ut enim ad minima veniam.
            </p>
            <h3>Lorem ipsum dolor sit amet H3</h3>
            <h4>Lorem ipsum dolor sit amet H4</h4>
            <h5>Lorem ipsum dolor sit amet H5</h5>

            <h3 class="blog-detail-dash-heading">Odrážkový seznam</h3>
            <ul>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
            </ul>

            <h3 class="blog-detail-dash-heading">Číselný seznam</h3>
            <ol>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
                <li>Anena ahlapa karta fala na tahnu sedart. Duis mollis, est non commondo luctas.</li>
            </ol>
        </div>





    </div>
</div>

<div class="blog">

    <div class="container">
        <div class="row">
            <h3>Články, které by se vám mohly líbit</h3>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card blog--card__first">

                <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="images/blog-foto.png">
                <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                <div class="blog--card__date">
                    <p>2.3.2018</p>
                </div>

                <div class="blog--card__signature">
                    <p>Petr Kott</p>
                </div>

            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 blog--card blog--card__second">

                <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="images/blog-foto2.png">
                <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                <div class="blog--card__date">
                    <p>2.3.2018</p>
                </div>

                <div class="blog--card__signature">
                    <p>Petr Kott</p>
                </div>

            </div>



            <div class="col-lg-4 blog--card blog--card__third">

                <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="images/blog-foto3.png">
                <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                <div class="blog--card__date">
                    <p>2.3.2018</p>
                </div>

                <div class="blog--card__signature">
                    <p>Petr Kott</p>
                </div>

            </div>


            <div class="col-lg-12">
                <a href="/blog.php"><button class="hp--blog-button">Více článků</button></a>
            </div>
        </div>
    </div>
</div>

<footer class="secondary-footer">

    <?php include "parts/category-short-contact.php"; ?>


</footer>
<?php include "footer.php"; ?>
