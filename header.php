<!doctype html>

<html class="no-js" lang="en">
<head>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#3e3e3e">
    <meta name="theme-color" content="#ffffff">



    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjHsSALqSFj8Wle0NSvx0YCggt01gP_iY">
    </script>
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>
    <script>
        document.documentElement.className = document.documentElement.className.replace("no-js","js");
    </script>

    <!-- Google Analytics -->
<!--    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-85624823-1', 'auto');
        ga('send', 'pageview');
    </script>-->

    <script type="text/javascript">
        /* <![CDATA[ */
        var seznam_retargeting_id = 98484;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//c.imedia.cz/js/retargeting.js"></script>

    <!-- End Google Analytics -->

    <!-- Global site tag (gtag.js) - Google Ads: 817153571 -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async
            src="https://www.googletagmanager.com/gtag/js?id=UA-85624823-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-85624823-1');
        gtag('config', 'AW-817153571');
    </script>

</head>
<body>
<main class="all">