<div class="short-contact">
    <div class="row">
        <div class="container nopadding">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 short-contact--phone">
                <a href="tel:<?php the_field('telefon', 'options') ?>"><span class="short-contact_svg"><?php get_template_part('svg/ico', 'tel') ?></span><span><?php the_field('telefon', 'options') ?></span></a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 short-contact--mail">
                <a href="mailto:<?php the_field('email', 'options') ?>"><span class="short-contact_svg"><?php get_template_part('svg/ico', 'mail') ?></span><span><?php the_field('email', 'options') ?></span></a>
            </div>
        </div>
    </div>

</div>



<!--
file_get_contents je tam dvakrát kvůli tomu, že to není na wordpressu a cesta musí být relativní, na podstránkách se používá jedno a na hlavní straně druhý.
Po napojení na wordpress to tam nebude.
-->