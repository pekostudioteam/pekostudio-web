

<div class="container subpage contact-page" id="mail">
    <div class="what-next" >
        <div class="container-shorter">
            <h2>Co dále?</h2>
            <p class="other_p"><?php the_field('prvni_odstavec', 'options') ?></p>
            <p class="other_p"><?php the_field('druhy_odstavec', 'options') ?></p>
        </div>



        <?php get_template_part('parts/function', 'mail') ?>



        <div class="container-shorter contact-form">
            <?php echo do_shortcode("[contact-form-7 id=\"375\" title=\"Kontaktní formulář 1\"]") ?>
            <p class="gdpr-p"><?php the_field('gdpr_veta', 'options') ?></p>
        </div>
    </div>
</div>





