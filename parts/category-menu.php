<div class="fixed-menu-all">
    <div class="fixed-menu">
        <div class="header-logo margintop30">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <span class="logo-svg"><?php get_template_part('svg/ico', 'logo_symbol') ?></span><span class="logo-svg-text logo-svg-text__header">PEKO STUDIO</span>
            </a>
        </div>
        <span onclick="menuhover()" id="mobile-menu-button"><a class="menu-icons" href="#"></a></span>
        <span onclick="menuhover()" class="menu-text">Menu</span>

    </div>

    <div class="main-menu">



        <div class="menu-arrow"></div>

        <?php
        wp_nav_menu( array(
            'theme_location' => 'primary',
            'depth'          => 2

        ) );
        ?>



    </div>

</div>






