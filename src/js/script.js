$(() => {
	// Navbar Toggler
	const nav = $('#nav')
	const menuToggler = $('#menuToggler')
	const menuTogglerText = menuToggler.find('span')
	console.log('menu toggler', menuToggler)
	console.log('menu toggler text', menuTogglerText)
	let menuActive = false
	let screenWidth = $(window).width()

	$(window).resize(() => {
		screenWidth = $(window).width()
	})

	const openMenu = () => {
		console.log('open menu')
		console.log('nav element', nav)

		if (menuActive) {
			console.log('menu is active -> close')
			nav.removeClass('expanded')
			menuToggler.removeClass('active')
			menuTogglerText.text('menu')
		} else {
			console.log('menu is not active -> open')
			nav.addClass('expanded')
			menuToggler.addClass('active')
			menuTogglerText.text('zavřít')
		}

		menuActive = !menuActive
	}

	menuToggler.click(() => openMenu())
	// End Of Navbar Toggler

	// Navbar Scroll
	const navHeight = nav.innerHeight()

	$(window).scroll(() => {
		const scrollTop = $(window).scrollTop()
		// if (scrollTop >= navHeight) {
		if (scrollTop > 0) {
			nav.addClass('scroll')
		} else {
			nav.removeClass('scroll')
		}
	})
	// End Of Navbar Scroll

	// Cookies
	analyticsAllowed = false
	marketingAllowed = false

	const allowAnalytics = () => {
		if (analyticsAllowed) {
			return
		}
		let cookieItem
		console.log('allow analytics')
		// cookieItem = document.createElement('script')
		// cookieItem.type = "text/javascript"
		// cookieItem.setAttribute('async', true)
		// cookieItem.src = 'https://www.googletagmanager.com/gtag/js?id=UA-161648927-1'
		// document.head.appendChild(cookieItem)

		// cookieItem = document.createElement('script')
		// cookieItem.innerHTML = "  window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}  gtag('js', new Date());  gtag('config', 'UA-161648927-1');"
		// document.head.appendChild(cookieItem)

		analyticsAllowed = true
	}

	const allowMarketing = () => {
		if (marketingAllowed) {
			return
		}
		let cookieItem
		console.log('allow marketing')
		// cookieItem = document.createElement('meta')
		// cookieItem.name = "facebook-domain-verification"
		// cookieItem.content = 'jucuk3dwxfn7eablvjpozhesdoejqs';
		// document.head.appendChild(cookieItem)
		marketingAllowed = true
	}

	let enabledAll = false
	const cookieArr = [
		{
			name: 'normal',
			expanded: false,
			toggled: true,
		},
		{
			name: 'analytics',
			expanded: false,
			toggled: false,
		},
		{
			name: 'marketing',
			expanded: false,
			toggled: false,
		},
	]

	const cookiesIcon = $('.newpopup--cookies')
	const cookiesCustomize = $('.cookies-customize')
	const cookiesOptions = $('.cookies-options')
	const cookiesModalWrapper = $('.cookies-modal')
	const cookiesModal = cookiesModalWrapper.find('.modal')
	const allowAll = $('.allow-all')
	const allowChosen = $('.allow-chosen')
	let cookiesActive = false
	let cookiesActive2 = false

	const setCookies = () => {
		window.localStorage.setItem('cookies', JSON.stringify({
			items: cookieArr,
			enabledAll,
		}))
	}

	const loadCookies = () => {
		const cookiesChecked = JSON.parse(window.localStorage.getItem('cookies'))

		if (cookiesChecked) {
			cookiesChecked.items.forEach((item, i) => {
				cookieArr[i] = item
			})
			enabledAll = cookiesChecked.enabledAll
		}

		cookieArr[0].toggled = true

		if (cookieArr[1].toggled) {
			allowAnalytics()
		}

		if (cookieArr[2].toggled) {
			allowMarketing()
		}

		if (!enabledAll) {
			cookiesIcon.show()
			cookiesOptions.show()
			cookiesModalWrapper.show()
		}
	}

	loadCookies()

	const setModal = (bool) => {
		if (bool) {
			cookiesModal.addClass('active')
			cookiesModalWrapper.addClass('active')
		} else {
			cookiesModal.removeClass('active')
			cookiesModalWrapper.removeClass('active')
		}
	}

	cookiesIcon.click(function () {
		cookiesActive = !cookiesActive
		if (cookiesActive) {
			cookiesOptions.addClass('active')
		} else {
			cookiesOptions.removeClass('active')
		}
	})

	cookiesCustomize.click(function () {
		cookiesActive2 = !cookiesActive2
		setModal(cookiesActive2)
	})

	allowAll.click(function () {
		cookiesActive = false
		cookiesActive2 = false
		cookiesOptions.removeClass('active')
		setModal(false)
		cookieArr.forEach(item => item.toggled = true)
		enabledAll = true
		allowAnalytics()
		allowMarketing()
		setCookies()
		cookiesIcon.remove()
	})

	allowChosen.click(function () {
		cookiesActive = false
		cookiesActive2 = false
		cookiesOptions.removeClass('active')
		setModal(false)
		// cookiesIcon.remove()
	})

	const handleWindowClick = (e) => {
		if (cookiesActive2) {
			const domList = e.target.classList

			for (let i = 0; i < domList.length; i++) {
				if (domList[i] === "modal-wrapper") {
					cookiesActive2 = false
					setModal(cookiesActive2)
				}
			}
		}
	}

	window.addEventListener("mousedown", handleWindowClick)

	const cookieItems = $('.modal-option-item')

	cookieItems.each(function (i) {
		const item = $(this)
		const expander = item.find('.moit-left')
		const toggler = item.find('.moit-button')

		// if (cookieArr[i].expanded) {
		// 	item.addClass('expanded')
		// }
		if (cookieArr[i].toggled) {
			item.addClass('active')
		}

		expander.click(function () {
			cookieItems.each(function (j) {
				if (i != j) {
					cookieArr[j].expanded = false
					$(this).removeClass('expanded')
				}
			})
			cookieArr[i].expanded = !cookieArr[i].expanded

			if (cookieArr[i].expanded) {
				item.addClass('expanded')
			} else {
				item.removeClass('expanded')
			}

			setCookies()
		})

		toggler.click(function () {
			if (cookieArr[i].name == 'normal') {
				return
			}

			cookieArr[i].toggled = !cookieArr[i].toggled
			enabledAll = false

			if (cookieArr[i].toggled) {
				item.addClass('active')
				if (cookieArr[i].name == 'analytics') {
					allowAnalytics()
				}
				if (cookieArr[i].name == 'marketing') {
					allowMarketing()
				}
			} else {
				item.removeClass('active')
			}

			setCookies()
		})
	})

	// WordPress submits
	const wpcfSubmits = $('.wpcf7-submit')

	if (wpcfSubmits.length) {
		wpcfSubmits.each(function () {
			$(this).val('')
			const parent = $(this).parent()
			parent.addClass('wpcf-submit-parent')
			parent.append('<div class="wpcf-submit-bg"></div>')
			parent.append('<p class="wpcf-submit-value">Odeslat</p>')
		})
	}

	// Colored Logos
	const fpLogos = $('.coop-section__companies')
	const coopLogos = $('.cooperation-section__list')

	const coloredLogoes = (element) => {
		const scrollTop = $(window).scrollTop()

		if (scrollTop >= element.offset().top - $(window).height() * 0.8 && scrollTop <= element.offset().top - $(window).height() * 0.2) {
			element.addClass('scrolled')
		} else {
			element.removeClass('scrolled')
		}
	}

	if (fpLogos.length) {
		$(window).scroll(() => {
			coloredLogoes(fpLogos)
		})
	}

	if (coopLogos.length) {
		$(window).scroll(() => {
			coloredLogoes(coopLogos)
		})
	}

	// End Of Colored Logos

	// Reviews Slideshow
	const reviewsSliderMobile = $('#reviewsSliderMobile')
	const reviewsSliderDesktop = $('#reviewsSliderDesktop')

	const setupReviewsSlider = () => {
		reviewsSliderMobile.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			vertical: true,
			verticalSwiping: true,
			prevArrow: $('#sliderArrowPrevMobile'),
			nextArrow: $('#sliderArrowNextMobile'),
			appendDots: $('#reviewsSliderDotsMobile'),
		})
		reviewsSliderDesktop.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			vertical: true,
			verticalSwiping: true,
			prevArrow: $('#sliderArrowPrev'),
			nextArrow: $('#sliderArrowNext'),
			appendDots: $('#reviewsSliderDots'),
		})
	}

	if (reviewsSliderMobile.length || reviewsSliderDesktop.length) {
		setupReviewsSlider()
	}
	// End Of Reviews Slideshow

	// Reference Detail Fixed Bar
	const bar = $('#bar') // helper element for original bar position before its fixed

	if (bar.length) {
		const referenceDetailBar = $('#barHeight') // bar
		const topContent = $('.reference-detail-top') // whole top content

		$(window).scroll(() => {
			const scrollTop = $(window).scrollTop()
			const barOffset = bar.offset().top

			// If scrollTop is higher than barOffset (bar offset from top)
			// - navbarHeight - 20px (margin between bar and navbar) add fixed
			// class to bar and add additional padding to whole top content
			// to fill space with missing bar
			if (scrollTop > barOffset - navHeight - 10) {
				topContent.addClass('fixedBar')
			} else {
				topContent.removeClass('fixedBar')
			}
		})
	}
	// End Of Reference Detail Fixed Bar

	// Why Us Section Colored Headings
	const whyUs = $('#whyUs')

	if (whyUs.length) {
		const whyUsItems = $('.wusi__item')

		$(window).scroll(() => {
			const scrollTop = $(window).scrollTop()

			whyUsItems.each(function () {
				const item = $(this)
				const itemTitle = item.find('.wusi-item__title')

				// if scrollTop is higher than item offset - 60% * windowHeight
				// => make it colored 
				if (scrollTop > item.offset().top - window.innerHeight * 0.6) {
					itemTitle.addClass('active')
				}
			})
		})
	}
	// End Of Why Us Section Colored Headings

	// How We Do It Shader
	const shader = $('#shader')
	const hwdi = $('.hwdi-section')

	if (shader.length && hwdi.length) {
		const oval = $('#Oval')
		let hwdiTop = hwdi.offset().top
		let hwdiHeight = hwdi.height()

		const setSvg = () => {
			const scrollTop = $(window).scrollTop()
			const scrollSectionTop = scrollTop - hwdiTop + $(window).height() / 2

			const perc = (scrollSectionTop / hwdiHeight) * 100

			let percX = 0
			const breakpoint = 40

			if (perc < breakpoint) {
				percX = 100 - perc
			} else {
				percX = perc * ((100 - breakpoint) / breakpoint)
			}

			percX = percX - 35

			oval.css('cx', `${percX}%`)
			oval.css('cy', `${perc}%`)
		}

		$(window).scroll(() => {
			setSvg()
		})

		$(window).resize(() => {
			hwdiTop = hwdi.offset().top
			hwdiHeight = hwdi.height()
		})

		setSvg()
	}
	// End Of How We Do It Shader

	// Technology Images Animation
	const technologyImages = $('#technologyImages')

	if (technologyImages.length) {
		let alreadyAnimated = false

		$(window).scroll(() => {
			const scrollTop = $(window).scrollTop()
			const offsetTop = technologyImages.offset().top

			// animate image when scroll reaches technology section
			if (!alreadyAnimated && scrollTop > offsetTop - window.innerHeight * 0.6) {
				alreadyAnimated = true
				let images = $('.tsml__item')
				if (!images.length) {
					images = $('.rts-list__item')
				}

				let delay = 0

				images.each(function () {
					const img = $(this).find('img')
					delay += 100

					img.animate({
						opacity: '1'
					}, delay)
				})
			}
		})
	}
	// End Of Technology Images Animation

	// Reference Images Animation

	// TODO: CELĚ PŘEDĚLAT?

	// End Of Reference Images Animation

	// About Us Scroll Section
	const scrollSection = $('.scroll-section')

	if (scrollSection.length) {
		const container = scrollSection.find('.container')
		const itemsContainer = $('.scroll-section-items')
		const items = $('.scroll-section__item')
		const scrollHeight = 500

		const isScrolledTop = (scrollTop) => {
			return scrollTop > scrollSection.offset().top ? true : false
		}

		const isScrolledBottom = (scrollTop) => {
			return scrollTop < scrollSection.offset().top + scrollSection.height() - window.innerHeight ? false : true
		}

		const isScrolledStep = (scrollTop) => {
			return scrollTop > scrollSection.offset().top + scrollHeight ? true : false
		}

		const isScrollWithinItem = (scrollTop, i) => {
			return scrollTop > scrollSection.offset().top + scrollHeight * (i + 1) && scrollTop < scrollSection.offset().top + scrollHeight * (i + 2) ? true : false
		}

		const resetClasses = () => {
			container.removeClass('fixed')
			container.removeClass('bottom')
			itemsContainer.removeClass('step')
			items.each(function () {
				$(this).removeClass('active')
			})
			scrollSection.height('auto')
		}

		const execute = () => {
			scrollSection.height(window.innerHeight + scrollHeight * (items.length + 1))
			const scrollTop = $(window).scrollTop()

			// If scroll is within section make container fixed
			if (isScrolledTop(scrollTop) && !isScrolledBottom(scrollTop)) {
				container.addClass('fixed')
			} else {
				container.removeClass('fixed')
			}

			// If scroll passed the section, make container absolute to bottom
			if (isScrolledBottom(scrollTop)) {
				container.addClass('bottom')
			} else {
				container.removeClass('bottom')
			}

			// First step - align items to left and lower opacity
			if (isScrolledStep(scrollTop)) {
				itemsContainer.addClass('step')
			} else {
				itemsContainer.removeClass('step')
			}

			items.each(function (i) {
				if (isScrollWithinItem(scrollTop, i)) {
					$(this).addClass('active')
				} else {
					$(this).removeClass('active')
				}
			})
		}

		$(window).scroll(() => {
			if ($(window).innerWidth() > 992) {
				execute()
			} else {
				resetClasses()
			}
		})

		$(window).resize(() => {
			if ($(window).innerWidth() > 992) {
				execute()
			} else {
				resetClasses()
			}
		})

		if ($(window).innerWidth() > 992) {
			execute()
		} else {
			resetClasses()
		}
	}

	// if (scrollSection.length) {
	// 	const items = $('.scroll-section__item')
	// 	const itemContainer = $('.scroll-section-item-container')
	// 	const itemContainerHeight = itemContainer.height()
	// 	const scrollSection = $('.scroll-section')
	// 	const scrollSectionTop = scrollSection.offset().top

	// 	const offsets = []
	// 	const scrollActive = 350
	// 	const scrollHeight = $(window).height() * 0.5
	// 	for (let i = 0; i < items.length; i++) {
	// 		offsets.push(scrollSectionTop + scrollActive + scrollHeight * i)
	// 	}

	// 	const toTheLeftOffset = 0

	// 	$(window).scroll(() => {
	// 		const scrollTop = $(window).scrollTop()

	// 		// Fixed
	// 		if (scrollTop > scrollSectionTop - $(window).height() * 0.3) {
	// 			itemContainer.addClass('fixed container')
	// 		} else {
	// 			itemContainer.removeClass('fixed container')
	// 		}

	// 		// Left
	// 		if (scrollTop > scrollSectionTop) {
	// 			itemContainer.addClass('left')
	// 		} else {
	// 			itemContainer.removeClass('left')
	// 		}

	// 		// Left Active
	// 		if (scrollTop > scrollSectionTop + scrollActive) {
	// 			itemContainer.addClass('leftActive')
	// 		} else {
	// 			itemContainer.removeClass('leftActive')
	// 			items.each(function () {
	// 				$(this).removeClass('active')
	// 			})
	// 		}

	// 		offsets.forEach((offset, i) => {
	// 			if (scrollTop > offset && scrollTop < offset + scrollHeight) {
	// 				items.each(function () {
	// 					$(this).removeClass('active')
	// 				})
	// 				items.eq(i).addClass('active')
	// 			}
	// 		})
	// 	})
	// }
	// End Of About Us Scroll Section

	// Career
	const careerSections = $('.career-section')

	if (careerSections.length) {
		$(window).scroll(() => {
			const scrollTop = $(window).scrollTop()

			careerSections.each(function () {
				const bg = $(this).find('.career-bg')

				if (scrollTop > $(this).offset().top - window.innerHeight * 0.5 && scrollTop < $(this).offset().top + window.innerHeight * 0.5) {
					bg.addClass('scrolled')
				} else {
					bg.removeClass('scrolled')
				}
			})
		})

	}

	const careerChallenges = $('.career-challenges')

	if (careerChallenges.length) {
		const careerItems = $('.career-challenges-list__item')

		$(window).scroll(() => {
			const scrollTop = $(window).scrollTop()

			careerItems.each(function () {
				if (scrollTop > $(this).offset().top - window.innerHeight * 0.5 && scrollTop < $(this).offset().top + window.innerHeight * 0.5) {
					$(this).addClass('active')
				} else {
					$(this).removeClass('active')
				}
			})
		})
	}

	// End Of Career

	// About Us Slideshow
	const teamSlider = $('#teamSlider')

	if (teamSlider.length) {
		const dots = $('.team-section-dot-wrapper')

		teamSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			prevArrow: $('#teamSliderArrowPrev'),
			nextArrow: $('#teamSliderArrowNext'),
		})

		dots.each(function (i) {
			const dot = $(this).find('.team-section-dots__dot')
			dot.click(() => goToSlickPage(i))
		})

		const setDots = index => {
			dots.removeClass('active')
			$(dots[index]).addClass('active')
		}

		const goToSlickPage = index => {
			teamSlider.slick('slickGoTo', index)
		}

		teamSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			setDots(nextSlide)
		})

		goToSlickPage(0)
	}
	// End Of About Us Slideshow

	// Dev Slides Section
	const devSlides = $('.dev-slides')

	if (devSlides.length && screenWidth > 992) {
		const slideItem = $('.dev-slide')
		const progressBar = $('#devProgressBar')
		let progressBarHeight = $('#devProgressBarBg').height()

		const progressItems = $('.dspi__item')
		const contentItems = $('.dsc__item')
		const imageItems = $('.dsil__item')
		const imageTexts = $('.dev-slides-images-text__item')
		const numberOfSlides = progressItems.length

		let slidesSectionOffset = devSlides.offset().top
		let slideHeight = slideItem.height()
		const scrollHeight = 500

		const slideOffsets = []
		for (let i = 0; i < numberOfSlides; i++) {
			slideOffsets.push(slidesSectionOffset + scrollHeight * i)
		}

		// set height to amount of slides * height of 1 slide
		let totalHeight = slideHeight + scrollHeight * numberOfSlides - scrollHeight / 2
		devSlides.height(totalHeight)

		// first dot will be always scrolled (colored)
		progressItems.eq(0).addClass('scrolled')

		// set first items to active if scrolling from top
		if ($(window).scrollTop() < slidesSectionOffset) {
			progressItems.eq(0).addClass('active')
			imageTexts.eq(0).addClass('active')
		}

		// set last items to active if scrolling from bottom
		if ($(window).scrollTop() > slidesSectionOffset + totalHeight) {
			imageTexts.eq(imageTexts.length - 1).addClass('active')
			contentItems.eq(contentItems.length - 1).addClass('active')
			imageItems.eq(imageItems.length - 1).addClass('active')
			progressItems.eq(progressItems.length - 1).addClass('active')
		}

		$(window).scroll(() => {
			scrollSlides()
		})

		$(window).resize(() => {
			slideHeight = slideItem.height()
			totalHeight = slideHeight + scrollHeight * numberOfSlides - scrollHeight / 2
			slidesSectionOffset = devSlides.offset().top
			progressBarHeight = $('#devProgressBarBg').height()
			if ($(window).width() > 992) {
				devSlides.height(totalHeight)
			} else {
				devSlides.height('auto')
			}

			scrollSlides()
		})

		const scrollSlides = () => {
			const scrollTop = $(window).scrollTop()

			// set progress bar % bg
			const setProgressBar = (() => {
				const slideScrollTop = scrollTop - slidesSectionOffset
				const slideEnd = scrollHeight * (numberOfSlides - 1)
				const percentage = slideScrollTop / slideEnd < 1 ? slideScrollTop / slideEnd : 1
				progressBar.height(progressBarHeight * percentage)
			})()

			// set slide to fixed if scroll is in section
			if (
				scrollTop > slidesSectionOffset &&
				scrollTop < slidesSectionOffset + totalHeight - slideHeight
			) {
				slideItem.addClass('fixed')
			} else {
				slideItem.removeClass('fixed')
			}

			if (scrollTop > slidesSectionOffset + totalHeight - slideHeight) {
				slideItem.css('bottom', 0)
				slideItem.css('top', 'unset')
			} else {
				slideItem.css('top', 0)
				slideItem.css('bottom', 'unset')
			}

			// set active slides based on scroll
			slideOffsets.forEach((offset, i) => {
				// check if scroll is within slide
				if (scrollTop > offset && scrollTop < offset + scrollHeight) {
					progressItems.eq(i).addClass('active')
					contentItems.eq(i).addClass('active')
					imageItems.eq(i).addClass('active')
					imageTexts.eq(i).addClass('active')
				} else {
					// don't remove active class if it's first/last item 
					// and scrollTop is ouf of the section
					if (
						!((scrollTop > slidesSectionOffset + totalHeight - slideHeight) && i == progressItems.length - 1)
						&& !((scrollTop < slidesSectionOffset) && i == 0)
					) {
						progressItems.eq(i).removeClass('active')
						contentItems.eq(i).removeClass('active')
						imageItems.eq(i).removeClass('active')
						imageTexts.eq(i).removeClass('active')
					}
				}

				// check if scroll hit slide
				if (scrollTop > offset) {
					progressItems.eq(i).addClass('scrolled')
				} else {
					// first dot will be always active (scrolled)
					if (i != 0) {
						progressItems.eq(i).removeClass('scrolled')
					}
				}

				// check if scroll passed slide
				if (scrollTop > offset + scrollHeight) {
					// don't scroll last item
					if (i != progressItems.length - 1) {
						imageItems.eq(i).addClass('scrolled')
					}
				} else {
					imageItems.eq(i).removeClass('scrolled')
				}
			})
		}

		scrollSlides()
	}

	// End Of Dev Slides Section

	// About Counter
	const counterSection = $('.company-section')

	if (counterSection.length) {
		const counters = $('.company-section-item__number')
		const duration = 500
		let animated = false

		const counterAnimation = () => {
			if ($(window).scrollTop() > counterSection.offset().top - $(window).height() / 2 && !animated) {

				counters.each(function () {
					const current = $(this)
					let val = 0
					const finalVal = parseInt(current.text())
					current.text(`${val}+`)

					const addCount = () => {
						if (val != finalVal) {
							setTimeout(() => {
								val += 1
								current.text(`${val}+`)
								addCount()
							}, duration / finalVal)
						}
					}

					addCount()
				})

				animated = true
			}
		}

		$(window).scroll(() => {
			counterAnimation()
		})

		counterAnimation()
	}
	// End Of About Counter
})

$(document).ready(function () {
	if ($('#device').length > 0) {
		$(window).scroll(function () {
			scrollFc()
		})

		const scrollFc = () => {
			if ($(window).width() > 768) {
				var scroll = $(document).scrollTop()
				var height = $('#device').height()
				var offsetTop = $('#device').offset().top

				if (scroll > offsetTop - (height)) {
					$('.reference-images-wrapper').addClass('reference-img-phone--content-anim')
				}
				// if (scroll < offsetTop - (height)) {
				// 	$('.reference-images-wrapper').removeClass('reference-img-phone--content-anim')
				// }
			} else {
				$('.reference-images-wrapper').addClass('reference-img-phone--content-anim')
			}
		}

		scrollFc()
	}
})


$(document).ready(function () {
	var windowWidth = $(window).width()
	if (windowWidth > 768) {
		if ($('#peko').length > 0) {
			$(window).load(function () {


				var offsetP = $('#letterP').offset().left
				var offsetE = $('#letterE').offset().left
				var offsetK = $('#letterK').offset().left
				var offsetO = $('#letterO').offset().left

				var widthP = $('#letterP').width()
				var widthE = $('#letterE').width()
				var widthK = $('#letterK').width()
				var widthO = $('#letterO').width()


				$('#letterP').css('left', offsetP - widthP + 45 + 'px')
				$('#letterE').css('left', offsetE - widthE + 45 + 'px')
				$('#letterK').css('left', offsetK - widthK + 5 + 'px')
				$('#letterO').css('left', offsetO - widthO + 45 + 'px')
				$('.about-peko-letter-wrap').css('position', 'absolute')

			})
			$(window).scroll(function () {
				var scrollTop = $(document).scrollTop()
				var height = $(window).height()
				var offsetTopPeko = $('#peko').offset().top
				var heightPeko = $('.about-peko-word').height()


				var offsetTop = $('#aboutPeko').offset().top

				var widthP = $('#letterP').width()
				var widthE = $('#letterE').width()
				var widthK = $('#letterK').width()
				var widthO = $('#letterO').width()

				if ((scrollTop - offsetTop + 200) > 0) {
					$('.about-peko-letter-wrap').addClass('about-peko-letter-wrap--null')
					$('#letterP').css('margin-left', 15 + 'px')
					$('#letterE').css('margin-left', widthP + 30 + 'px')
					$('#letterK').css('margin-left', widthP + widthE + 45 + 'px')
					$('#letterO').css('margin-left', widthP + widthE + widthK + 60 + 'px')
					$('.about-peko-letter--fill').css('opacity', '0')
				} else {
					$('.about-peko-letter-wrap').removeClass('about-peko-letter-wrap--null')
					$('#letterP').css('margin-left', 0 + 'px')
					$('#letterE').css('margin-left', 0 + 'px')
					$('#letterK').css('margin-left', 0 + 'px')
					$('#letterO').css('margin-left', 0 + 'px')
					$('.about-peko-letter--fill').css('opacity', '1')
				}


				var show = 300
				if ((scrollTop - offsetTop + 200) > (show / 4)) {
					$('.about-peko-letter').css('opacity', '0.1')
					$('.about-peko-letter--fill').css('opacity', '0')
				} else {
					$('.about-peko-letter').css('opacity', '1')
				}

				if ((scrollTop - offsetTop + 200) > show) {
					$('.about-peko--content-first').addClass('about-peko--content--active')
				} else {
					$('.about-peko--content-first').removeClass('about-peko--content--active')
				}

				if ((scrollTop - offsetTop + 200) > (show * 2)) {
					$('.about-peko--content-first').removeClass('about-peko--content--active')
					$('.about-peko--content-second').addClass('about-peko--content--active')
				} else {
					$('.about-peko--content-second').removeClass('about-peko--content--active')
				}

				if ((scrollTop - offsetTop + 200) > (show * 3)) {
					$('.about-peko--content-second').removeClass('about-peko--content--active')
					$('.about-peko--content-third').addClass('about-peko--content--active')
					// $('#letterK .about-peko--title').css('left', 160 + 'px')
				} else {
					$('.about-peko--content-third').removeClass('about-peko--content--active')
				}

				if ((scrollTop - offsetTop + 200) > (show * 4)) {
					$('.about-peko--content-third').removeClass('about-peko--content--active')
					$('.about-peko--content-fourth').addClass('about-peko--content--active')
				} else {
					$('.about-peko--content-fourth').removeClass('about-peko--content--active')
				}

				if ((scrollTop - offsetTop + 200) > (show * 5)) {
					$('.about-peko--content-fourth').removeClass('about-peko--content--active')
				}






				if ((scrollTop + (height / 2) - (heightPeko / 2)) >= offsetTopPeko) {
					$('.about-peko-word').addClass('about-peko-word--fixed')
				} else {
					$('.about-peko-word').removeClass('about-peko-word--fixed')
				}

				var aboutPekoHeight = $('#aboutPeko').innerHeight()
				if (scrollTop > (aboutPekoHeight - 280)) {
					$('#aboutPeko').addClass('about-peko-word--absolute')
				} else {
					$('#aboutPeko').removeClass('about-peko-word--absolute')
				}

				//  if((scrollTop  + (height/2) + heightPeko) < offsetTop ){
				//     $('.about-peko-word').removeClass('about-peko-word--fixed')
				//   }

			})
		}
	}
})