<div class="footer--black">
    <div class="container footer--black__container">
        <div class="row footer--desktop">
            <div class="col-lg-4 col-md-4 footer--black__first">
                <span class="logo-svg logo-svg__footer"><?php get_template_part('svg/ico', 'logo_symbol') ?></span><span class="logo-svg-text logo-svg-text__footer">PEKO STUDIO</span>

            </div>

            <div class="col-lg-4 col-md-4 footer--black__second">
                <p><?php the_field('text_footer', 'options') ?><?php date("Y") ?></p>
            </div>

            <div class="col-lg-4 col-md-4 footer--black__third">
                <a href="<?php the_field('facebook', 'options') ?>"><span class="social_svg"><?php get_template_part('svg/ico', 'fb') ?></span></a>
                <!--<a href="<?php the_field('twitter', 'options') ?>"><span class="social_svg"><?php get_template_part('svg/ico', 'twitter') ?></span></a> -->
            </div>
        </div>

        <div class="row footer--tablet">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 footer--black__second">
                <p><?php the_field('text_footer', 'options') ?><?php date("Y") ?></p>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 footer--black__first">
                <span class="logo-svg logo-svg__footer"><?php get_template_part('svg/ico', 'logo_symbol') ?></span><span class="logo-svg-text logo-svg-text__footer">PEKO STUDIO</span>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 footer--black__third">
                <a href="<?php the_field('facebook', 'options') ?>"><span class="social_svg"><?php get_template_part('svg/ico', 'fb') ?></span></a>
                <a href="<?php the_field('twitter', 'options') ?>"><span class="social_svg"><?php get_template_part('svg/ico', 'twitter') ?></span></a>
            </div>
        </div>
    </div>
</div>

</main>



<?php wp_footer(); ?>
</body>

</html>
