<?php  ?>
<?php get_header(); ?>




    <header class="secondary-header">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1>Error</h1>
            </div>

        </div>
    </header>



<div class="container container-error">


    <img alt="logo-shadow" title="" src="<?php echo get_stylesheet_directory_uri() ?>/images/polygon_error.png" class="error-polygon">

    <div class="error-heading">
        <div class="error-type">Error</div>
        <span class="error-number">404</span><br>
        <span class="error-text">Stránka nenalezena</span>
    </div>

    <div class="error-body">
        <span class="error-message">Oooops.. Omlouváme se ale vypadá to, že požadovaná stránka neexistuje.<br>V případě dalších potíží nás prosím kontaktujte prostřednictvím formuláře níže.</span>
    </div>
</div>


    <div class="what-next__wrap-other">
        <?php get_template_part('parts/category', 'what-next') ?>

    </div>





    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>



<?php get_footer(); ?>