<svg width="35px" height="36px" viewBox="0 0 35 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 52 (66869) - http://www.bohemiancoding.com/sketch -->
    <title>Artboard</title>
    <desc>Created with Sketch.</desc>
    <g id="Artboard" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="iconmonstr-check-mark-1" transform="translate(1.000000, 5.000000)" fill="#F79D26">
            <polygon id="Path" points="27.891875 0 12.375 15.8285263 5.10675 8.97136842 0 14.0564211 12.375 26 33 5.08368421"></polygon>
        </g>
    </g>
</svg>