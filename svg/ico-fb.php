<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="18px" height="17.5px" viewBox="0 0 18 17.5" style="enable-background:new 0 0 18 17.5;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Forma_1_kopie_6_1_" class="st0">
        <g id="Forma_1_kopie_6">
            <g>
                <path d="M14.6,0H3.4C1.5,0,0,1.5,0,3.3v10.9c0,1.8,1.5,3.3,3.4,3.3h11.2c1.9,0,3.4-1.5,3.4-3.3V3.3C18,1.5,16.5,0,14.6,0z
				 M11.3,8.7H9.8v5.2h-2V8.7H6.5V6.9h1.3V5.8c0-1.5,0.4-2.4,2.2-2.4h1.5v1.8h-0.9C9.9,5.3,9.8,5.6,9.8,6v0.9h1.7L11.3,8.7z"/>
            </g>
        </g>
    </g>
</svg>
