<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="12.5px" height="15px" viewBox="0 0 12.5 15" style="enable-background:new 0 0 12.5 15;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Forma_1_9_" class="st0">
        <g id="Forma_1_4_">
            <g>
                <path d="M9.3,0C8.5,0,7.6,0.5,7.1,1.1s-1,1.5-0.9,2.4C7.1,3.5,8,3,8.5,2.4C9.1,1.8,9.4,0.9,9.3,0z M10.4,8c0-1.9,1.6-2.8,1.7-2.9
				c-0.9-1.3-2.3-1.5-2.8-1.5C8.1,3.5,7,4.3,6.4,4.3S4.9,3.6,3.9,3.6c-1.3,0-2.5,0.7-3.2,1.9c-1.4,2.3-0.3,5.7,1,7.6
				C2.3,14,3,15,4,15s1.3-0.6,2.5-0.6S8,15,9,15s1.7-0.9,2.4-1.8c0.7-1.1,1-2.1,1.1-2.1C12.5,11,10.5,10.2,10.4,8z"/>
            </g>
        </g>
    </g>
</svg>
