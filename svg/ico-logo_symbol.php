<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="50.6px" height="28.8px" viewBox="0 0 50.6 28.8" style="enable-background:new 0 0 50.6 28.8;" xml:space="preserve">
<style type="text/css">
    .logo0{fill:#F69C25;}
    .logo1{fill:#F17A29;}
    .logo2{fill:#BE2026;}
    .logo3{fill:#A51E22;}
    .logo4{fill:#E23C48;}
</style>
    <g>
        <polygon class="logo0" points="50.6,14.3 33.7,14.3 42.3,0 	"/>
        <polygon class="logo1" points="50.6,14.3 33.7,14.3 42.3,28.8 	"/>
        <polygon class="logo0" points="42.3,28.8 25.4,28.8 33.7,14.3 	"/>
        <polygon class="logo2" points="33.7,14.3 16.9,14.3 25.4,28.8 	"/>
        <polygon class="logo3" points="33.8,14.3 16.7,14.3 25.2,0 	"/>
        <polygon class="logo4" points="16.9,14.3 8.7,0 25.2,0 	"/>
        <polygon class="logo3" points="16.9,14.3 0,14.3 8.7,0 	"/>
    </g>
</svg>
