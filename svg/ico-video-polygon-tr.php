<svg width="309px" height="355px" viewBox="0 0 309 355" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
    <title>Polygon</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0">
        <g id="video-polygon" stroke="#444444" stroke-width="3">
            <polygon id="Polygon" points="154.5 0 308.219509 88.75 308.219509 266.25 154.5 355 0.78049083 266.25 0.78049083 88.75"></polygon>
        </g>
    </g>
</svg>