<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="19.5px" height="13px" viewBox="0 0 19.5 13" style="enable-background:new 0 0 19.5 13;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Forma_1_7_" class="st0">
        <g id="Forma_1_6_">
            <g>
                <path d="M19.5,11.1C19.5,11.1,19.4,11.1,19.5,11.1H0.1H0v0.1c0,0,0.3,1.8,2.6,1.8H17C19.2,13,19.5,11.2,19.5,11.1L19.5,11.1z
				 M11.2,12.4H8.3v-0.7h2.9C11.2,11.7,11.2,12.4,11.2,12.4z M2.1,10.3h15.3c0.2,0,0.4-0.2,0.4-0.4V0.4c0-0.2-0.2-0.4-0.4-0.4H2.1
				C1.9,0,1.7,0.2,1.7,0.4v9.5C1.7,10.2,1.9,10.3,2.1,10.3z M3.1,1.4h13.3v7.5H3.1V1.4z"/>
            </g>
        </g>
    </g>
</svg>
