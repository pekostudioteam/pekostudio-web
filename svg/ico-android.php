
<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="11px" height="16.5px" viewBox="0 0 11 16.5" style="enable-background:new 0 0 11 16.5;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Forma_1_10_" class="st0">
        <g id="Forma_1_3_">
            <g>
                <path d="M11,6.6c0-1.8-0.9-3.3-2.3-4.3l1.7-1.6L9.7,0L7.9,1.8C7.2,1.4,6.4,1.2,5.5,1.2S3.8,1.4,3.1,1.8L1.3,0L0.6,0.6l1.7,1.6
				C0.9,3.2,0,4.8,0,6.6v0.8h11V6.6z M3.1,5.8C2.7,5.8,2.4,5.4,2.4,5s0.4-0.8,0.8-0.8S4,4.5,4,5C3.9,5.4,3.6,5.8,3.1,5.8z M7.9,5.8
				C7.4,5.8,7.1,5.4,7.1,5s0.4-0.8,0.8-0.8S8.6,4.6,8.6,5S8.3,5.8,7.9,5.8z M0,8.1v3.1c0,3,2.5,5.4,5.5,5.4s5.5-2.4,5.5-5.4V8.1H0z"
                />
            </g>
        </g>
    </g>
</svg>
