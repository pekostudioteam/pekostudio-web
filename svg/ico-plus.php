<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="6px" height="6px" viewBox="0 0 6 6" style="enable-background:new 0 0 6 6;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Zaoblený_obdélník_4_kopie_1_" class="st0">
        <g id="Zaoblený_obdélník_4_kopie">
            <g>
                <path d="M5.5,2.5h-2v-2C3.5,0.2,3.3,0,3,0S2.5,0.2,2.5,0.5v2h-2C0.2,2.5,0,2.7,0,3s0.2,0.5,0.5,0.5h2v2C2.5,5.8,2.7,6,3,6
				s0.5-0.2,0.5-0.5v-2h2C5.8,3.5,6,3.3,6,3S5.8,2.5,5.5,2.5z"/>
            </g>
        </g>
    </g>
</svg>
