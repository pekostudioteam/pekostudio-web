<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="17px" height="8.5px" viewBox="0 0 17 8.5" style="enable-background:new 0 0 17 8.5;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Forma_1_12_" class="st0">
        <g id="Forma_1_1_">
            <g>
                <path d="M4,0L0.6,3c-0.7,0.6-0.7,1.7,0,2.4l3.4,3l1.1-1.3l-3.3-3l3.4-3L4,0z M16.4,3.1L13,0l-1.1,1.3l3.4,3l-3.3,3l1,1.2l3.4-3
				C17.2,4.8,17.2,3.7,16.4,3.1z M6.8,8.5h1.7L10.2,0H8.5L6.8,8.5z"/>
            </g>
        </g>
    </g>
</svg>
