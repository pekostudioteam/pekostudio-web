<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="22.5px" height="22.5px" viewBox="0 0 22.5 22.5" style="enable-background:new 0 0 22.5 22.5;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:#F27A2A;}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="Elipsa_1_kopie_5_1_" class="st0">
    </g>
    <g id="Forma_1_14_" class="st0">
        <g id="Forma_1_7_">
            <g>
                <path class="st1" d="M20,1.9c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4c0.2,0,0.4-0.2,0.4-0.4C20.5,2.1,20.3,1.9,20,1.9z M17.4,1.9
				c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4c0.2,0,0.4-0.2,0.4-0.4S17.6,1.9,17.4,1.9z M14.8,1.9c-0.2,0-0.4,0.2-0.4,0.4
				s0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4S15,1.9,14.8,1.9z M12.1,13.6c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4
				C12.6,13.8,12.4,13.6,12.1,13.6z M12.1,16.2c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4
				C12.6,16.4,12.4,16.2,12.1,16.2z M12.1,18.9c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4
				C12.6,19,12.4,18.9,12.1,18.9z M21.2,0H1.3C0.6,0,0,0.6,0,1.3v19.9c0,0.7,0.6,1.3,1.3,1.3h19.9c0.7,0,1.3-0.6,1.3-1.3V1.3
				C22.5,0.6,21.9,0,21.2,0z M21.6,21.2c0,0.2-0.2,0.4-0.4,0.4H1.3c-0.2,0-0.4-0.2-0.4-0.4V4.7h20.7V21.2z M21.6,3.8H0.9V1.3
				c0-0.2,0.2-0.4,0.4-0.4h19.9c0.2,0,0.4,0.2,0.4,0.4C21.6,1.3,21.6,3.8,21.6,3.8z M3.3,11.7h15.8l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0
				l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0c0,0,0,0,0.1-0.1l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0V6.9
				c0-0.2-0.2-0.4-0.4-0.4H3.3l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0
				l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0v4.4C2.9,11.5,3.1,11.7,3.3,11.7z M18.7,7.3v3.4L6.6,7.3H18.7z M3.8,7.4L16,10.8H3.8
				V7.4z M3.3,19.7h6.2l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0
				l0,0l0,0l0,0l0,0l0,0l0,0V14c0-0.2-0.2-0.4-0.4-0.4H3.3l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0
				l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0l0,0v5.3C2.9,19.5,3.1,19.7,3.3,19.7z M9.1,14.5v3.9l-4.5-3.9
				H9.1z M3.8,15l4.5,3.9H3.8V15z M19.2,13.6h-5.3c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4h5.3c0.2,0,0.4-0.2,0.4-0.4
				S19.4,13.6,19.2,13.6z M19.2,16.2h-5.3c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h5.3c0.2,0,0.4-0.2,0.4-0.4
				C19.6,16.4,19.4,16.2,19.2,16.2z M19.2,18.9h-5.3c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h5.3c0.2,0,0.4-0.2,0.4-0.4
				C19.6,19,19.4,18.9,19.2,18.9z"/>
            </g>
        </g>
    </g>
</svg>
