<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="11.5px" height="17px" viewBox="0 0 11.5 17" style="enable-background:new 0 0 11.5 17;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
</style>
    <g id="Forma_1_8_" class="st0">
        <g id="Forma_1_5_">
            <g>
                <path d="M9.3,4L8.6,0H2.9L2.2,4C0.9,5.1,0,6.7,0,8.5s0.9,3.4,2.2,4.5l0.7,4h5.7l0.7-4c1.3-1.1,2.2-2.7,2.2-4.5S10.6,5.1,9.3,4z
				 M5.8,12.8c-2.4,0-4.3-1.9-4.3-4.3c0-2.3,1.9-4.2,4.3-4.2s4.3,1.9,4.3,4.2S8.1,12.8,5.8,12.8z"/>
            </g>
        </g>
    </g>
</svg>
