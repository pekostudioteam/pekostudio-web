
<?php get_header(); ?>



<?php
if (get_field('align4', 'options')=="left"){
    $align="left";
}
else{
    $align="right";
}
?>
<header class="secondary-header ref-bg"   style="background: url(<?php the_field('ref_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
    <div class="header-shaddow">

        <div class="row content">
            <?php get_template_part('parts/category', 'menu') ?>
        </div>

        <div class="row content">
            <h1><?php the_title() ?> eee</h1>
        </div>

    </div>
</header>






<div class="container subpage reduce-pad-bot reference-page">
    <div class="filter-wrap">
        <span class="filter filter-title">Filtr:</span>
        <span class="filter filter-butt filter-ios" ><a href="#">iOS</a></span>
        <span  class="filter filter-butt filter-android"><a href="#">Android</a></span>
        <span  class="filter filter-butt filter-web"><a href="#">Webové aplikace</a></span>
    </div>
</div>









<div class="container-larger reduce-pad-bot subpage reference-page">
    <div class="row child-ref">

        <?php
        global $query_string;
        query_posts( $query_string . '&posts_per_page=9');
        ?>
        <?php  if ( have_posts()) while ( have_posts() ) : the_post(); ?>


            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                <a href="<?php the_permalink()?>">


                    <div class="ref-card">





                        <h2><?php the_field('nadpis_reference') ?></h2>

                        <span class="ref-what">

                            <?php if(get_field('android')){ ?>
                                Android
                                <?php if(get_field('android') && get_field('ios')){ ?>
                                    <?php if(get_field('android') && get_field('ios') && get_field('web')){ $was="1";?>
                                        ,
                                    <?php } else {?>
                                        a
                                    <?php }} ?>



                                <?php if(get_field('android') && get_field('web')){ ?>
                                    <?php if(get_field('android') && get_field('ios') && get_field('web')){?>

                                    <?php } else {?>
                                        a
                                    <?php }} ?>
                            <?php } ?>




                            <?php if(get_field('ios')){ ?>
                                iOS
                                <?php if(get_field('ios') && get_field('web')){?>
                                    a
                                <?php } }?>




                            <?php if(get_field('web')){ ?>
                                Webová
                            <?php } ?>




                            appka

                        </span>
                        <div class="ref-inclusion-wrap">

                            <?php if(get_field('hra')){ ?>
                                <span class="ref-inclusion">Hra</span>
                            <?php } ?>
                            <?php if(get_field('bussines')){ ?>
                                <span class="ref-inclusion">Bussines</span>
                            <?php } ?>
                            <?php if(get_field('osobni_rozvoj')){ ?>
                                <span class="ref-inclusion">Osobní rozvoj</span>
                            <?php } ?>

                        </div>

                        <?php if(get_field('desktop_mobile_video')=="mobil"){ ?>
                            <img class="ref-img" src="<?php the_field('ilustracni_fotografie') ?>">
                        <?php } ?>

                        <?php if(get_field('desktop_mobile_video')=="desktop"){ ?>
                            <img class="ref-img ref-img--desktop" src="<?php the_field('ilustracni_fotografie') ?>">
                        <?php } ?>
                    </div>
                </a>
            </div>

        <?php endwhile; ?>



        <div class="ref-button-wrap col-xs-12">
            <button class="ref-button">Více referencí</button>
        </div>

    </div>
</div>



<?php wp_reset_postdata() ?>
<?php wp_reset_query() ?>





<div class="what-next__wrap-other">
    <?php get_template_part('parts/category', 'what-next') ?>
</div>




<footer class="secondary-footer">
    <?php get_template_part('parts/category', 'short-contact') ?>
</footer>



<?php get_footer(); ?>