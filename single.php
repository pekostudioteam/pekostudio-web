<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php
    if (get_field('align2', 'options')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>
<header class="secondary-header blog-bg"    style="background: url(<?php the_field('blog_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
    <div class="header-shaddow">

        <div class="row content">
            <?php get_template_part('parts/category', 'menu') ?>
        </div>

        <div class="row content">
            <h1>Blog</h1>
        </div>

    </div>
</header>



<div class="container blog-detail-page">
    <div class="row">



        <div class="col-md-12 detail-blog-h2-phone">
            <h2 class="detail-blog--h2"><?php the_field('nadpis_clanku')?><br><?php the_field('podnadpis_clanku')?></h2>
        </div>


        <div class="col-xs-4 detail-blog--date date__phone">
            <p><?php the_field('datum')?></p>
        </div>

        <div class="col-xs-8 detail-blog--signature signature__phone">
            <p><?php the_field('autor_clanku')?></p>
        </div>


        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="single-blog-img-wrap">
                <img class="detail-blog--photo" alt="jak-urychlit-programovani-v-PHP-photo" src="<?php the_field('fotografie')?>">
            </div>
        </div>

        <div class="col-lg-7 col-md-7 col-sm-7">
            <div class="row">
                <div class="col-lg-12 detail-blog-h2-desktop">
                    <h2 class="detail-blog--h2"><?php the_field('nadpis_clanku')?><br><?php the_field('podnadpis_clanku')?></h2>
                </div>

                <div class="col-xs-4 detail-blog--date date__desktop">
                    <p><?php the_field('datum')?></p>
                </div>

                <div class="col-xs-8 detail-blog--signature signature__desktop">
                    <p><?php the_field('autor_clanku')?></p>
                </div>

            </div>
        </div>



        <div class="gray-p-blog col-xs-12">
            <div class="row">
            <div class="col-sm-5 col-xs-12">
            </div>
            <div class="col-sm-7 col-xs-12">
                <p class="detail-blog--p"><?php the_field('prvni_odstavec')?></p>
            </div>
            </div>
        </div>



        <div class="col-xs-12 blog-detail-content">

            <?php the_content()?>

        </div>





    </div>
</div>




<div class="blog">

    <div class="container">
        <div class="row child-hp-blog">
            <h3>Články, které by se vám mohly líbit</h3>





            <?php
            $categories = get_the_category($post->ID);
            if ( $categories ) {
                $category_ids = array();
                foreach ( $categories as $individual_category ) {
                    $category_ids[] = $individual_category->term_id;
                }
                $args=array(
                    'category__in' => $category_ids,
                    'post__not_in' => array($post->ID),
                    'showposts'=>3, // Number of related posts that will be shown.
                    'ignore_sticky_posts'=>1
                );
                $my_query = new wp_query( $args );
                if( $my_query->have_posts() ) {


                    while ($my_query->have_posts()) {
                        $my_query->the_post();
                        ?>


                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12  blog--card">
                            <a href="<?php the_permalink() ?>">
                                <div class="blog-img-wrap">
                                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php the_field('fotografie')?>">
                                </div>
                                <div class="blog-title-wrap">
                                    <span class="article-title">
                                        <?php the_field('nadpis_clanku')?>
                                        <br>
                                        <?php the_field('podnadpis_clanku')?>
                                    </span>
                                </div>



                                <p class="blog--p">
                                    <?php

                                    $maxLength = 110;
                                    $nazev = substr(get_field('prvni_odstavec'), 0, $maxLength);
                                    echo "$nazev..";

                                    ?>
                                </p>

                                <div class="blog--card__date">
                                    <p><?php the_field('datum')?></p>
                                </div>

                                <div class="blog--card__signature">
                                    <p><?php the_field('autor_clanku')?></p>
                                </div>
                            </a>
                        </div>


                        <?php
                    }


                }
            }
            ?>











            <div class="col-lg-12">
                <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-blog.php' ) ) ?>"><button class="hp--blog-button">Více článků</button></a>
            </div>
        </div>
    </div>
</div>



<footer class="secondary-footer">
    <?php get_template_part('parts/category', 'short-contact') ?>
</footer>





<?php get_footer(); ?>
<?php endwhile; ?>