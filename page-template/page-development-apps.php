<?php /* Template name: Development-apps */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <?php
    if (get_field('header_align')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>
    <header class="secondary-header dev-apps-bg"  style="background: url(<?php the_field('header_photo') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1><?php the_title() ?></h1>
            </div>

        </div>
    </header>



    <div class="development-wrap">
        <div class="container subpage container__reduce--bottom">
            <div class="container-shorter">
                <h2 class="dev-apps-h2"><?php the_field('nadpis') ?></h2>
                <p class="development-first-p"><?php the_field('uvodni_odstavec_1') ?> <br><?php the_field('uvodni_odstavec_2') ?></p>
            </div>

        </div>
        <div class="container-larger container__reduce--top subpage development-page">

            <div class="development-page-white">
                <h2><?php the_field('nadpis_procesu') ?></h2>
                <p class="dev-apps-p"><?php the_field('odstavec_procesu') ?></p>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dev-polygon"></div>
                    </div>
                    <div class="col-sm-6 dev-main dev-main__left">
                    </div>
                    <div class="col-sm-6 dev-main dev-main__right">
                    </div>



                    <div class="col-sm-6 dev dev__left">
                        <div class="dev-heading dev-heading__left">
                            <h3><?php the_field('nadpis_1') ?></h3>
                            <div class="dev-svg dev-svg__orange"><?php get_template_part('svg/ico', 'analysis') ?></div>
                        </div>
                        <p><?php the_field('text_1') ?></p>
                    </div>

                    <div class="col-sm-6 dev dev__right">
                        <div class="dev-heading dev-heading__right">
                            <div class="dev-svg dev-svg__dark-orange"><?php get_template_part('svg/ico', 'ux') ?></div>
                            <h3><?php the_field('nadpis_2') ?></h3>
                        </div>
                        <p><?php the_field('text_2') ?></p>
                    </div>



                    <div class="col-sm-6 dev dev__left">
                        <div class="dev-heading dev-heading__left">
                            <h3><?php the_field('nadpis_3') ?></h3>
                            <div class="dev-svg dev-svg__orange"><?php get_template_part('svg/ico', 'graphics') ?></div>
                        </div>
                        <p><?php the_field('text_3') ?></p>
                    </div>

                    <div class="col-sm-6 dev dev__right">
                        <div class="dev-heading dev-heading__right">
                            <div class="dev-svg dev-svg__red"><?php get_template_part('svg/ico', 'develop') ?></div>
                            <h3><?php the_field('nadpis_4') ?></h3>
                        </div>
                        <p><?php the_field('text_4') ?></p>
                    </div>




                    <div class="col-sm-6 dev dev__left">
                        <div class="dev-heading dev-heading__left">
                            <h3><?php the_field('nadpis_5') ?></h3>
                            <div class="dev-svg dev-svg__dark-orange"><?php get_template_part('svg/ico', 'testing') ?></div>
                        </div>
                        <p><?php the_field('text_5') ?></p>
                    </div>

                    <div class="col-sm-6 dev dev__right">
                        <div class="dev-heading dev-heading__right">
                            <div class="dev-svg dev-svg__dark-red"><?php get_template_part('svg/ico', 'support') ?></div>
                            <h3><?php the_field('nadpis_6') ?></h3>
                        </div>
                        <p><?php the_field('text_6') ?></p>
                    </div>




                    <div class="col-sm-6 dev-main dev-main__left">
                    </div>
                    <div class="col-sm-6 dev-main dev-main__right">
                    </div>
                    <div class="col-sm-12">
                        <div class="dev-polygon"></div>
                    </div>
                </div>
            </div>

            <?php if(get_field('obrazek_nahore')){ ?>
                <div class="ba-slider">
                    <img src="<?php the_field('obrazek_nahore') ?>" alt="">
                    <div class="resize">
                        <img src="<?php the_field('obrazek_dole') ?>" alt="">
                    </div>
                    <span class="handle"><span class="handle-sub-left"></span><span class="handle-sub"></span><span class="handle-sub-right"></span></span>
                </div>
            <?php } ?>


            <?php
            if(get_field('appka_web_mob')=="mobilni") {
                $parts = array('android', 'ios');
                ?>

                <div class="dev-apps-android-ios">

                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <a href="/tvorba-android-aplikaci">
                                <div class="android-wrap">
                                    <div class="dev-apps-android">
                                        <?php get_template_part('svg/ico', 'android') ?>
                                    </div>
                                    Tvorba Android aplikací
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <a href="/tvorba-ios-aplikaci">
                                <div class="apple-wrap">
                                    <div class="dev-apps-apple">
                                        <?php get_template_part('svg/ico', 'apple') ?>
                                    </div>
                                    Tvorba iOS aplikací
                                </div>
                            </a>
                        </div>
                    </div>

                </div>


                <?php
            }
            if(get_field('appka_web_mob')=="webova") {
                $parts = array('webove-aplikace');
                ?>

                <div class="dev-apps-android-ios">

                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <a href="/tvorba-webovych-aplikaci">
                                <div class="android-wrap">
                                    <div class="dev-apps-android">
                                        <?php get_template_part('svg/ico', 'web_apps') ?>
                                    </div>
                                    Tvorba webových aplikací
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <a href="/tvorba-webovych-stranek">
                                <div class="apple-wrap">
                                    <div class="dev-apps-apple dev-apps-page">
                                        <?php get_template_part('svg/ico', 'web_page') ?>
                                    </div>
                                    Tvorba webových stránek
                                </div>
                            </a>
                        </div>
                    </div>

                </div>

                <?php
            }
            ?>

        </div>
    </div>








    <div class="container subpage paddingbottom0 reference-page">
        <h2><?php the_field('tyto_aplikace')?></h2>


        <?php
        $terms = get_terms( 'type', array(
            'orderby'    => 'count',
            'hide_empty' => 0
        ) );



        echo '<div class="filter-wrap">';
        echo '<span class="filter filter-title">Typ:</span>';


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {
            if ($i == $len - 1) {
                echo '<span class="filter filter-main filter-main-last"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
            }
            else {
                echo '<span class="filter filter-main "><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
            }
            $i++;
        }


        echo '</div>';
        ?>




    </div>


    <div class="container-larger paddingbottom0 subpage reference-page">
        <div class="row child-ref child-vyvoj">












            <?php

            if(get_field('appka_web_mob')=="mobilni") {
                $parts = array('android', 'ios');
            }

            if(get_field('appka_web_mob')=="webova") {
                $parts = array('webove-aplikace');
            }





            $paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
            $args = array( 'post_type' => 'reference', 'posts_per_page' => 6, 'post_status' => 'published', 'paged' => $paged,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'platforms',
                        'field' => 'slug',
                        'terms' => $parts
                    )
                ));
            $loop = new WP_Query( $args );
            ?>

            <?php  while ( $loop->have_posts() ) :$loop->the_post();
                global $post;

                ?>



                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                    <a href="<?php the_permalink()?>">


                        <div class="ref-card">





                            <h2><?php the_field('nadpis_reference') ?></h2>

                            <span class="ref-what">


        <?php
        $terms = wp_get_post_terms( $post->ID,'platforms', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {


            if ($len > 1){
                if ($len > 2){
                    if ($i == 0) {
                        echo $term->name . ', ';
                    }
                    else if ($i == $len - 1) {
                        echo ' a ' . $term->name;
                    }

                    else {
                        echo $term->name;
                    }
                    $i++;
                }
                else {
                    if ($i == $len - 1) {
                        echo ' a ' . $term->name;
                    }
                    else {
                        echo $term->name;
                    }
                    $i++;

                }
            }



            else {
                echo $term->name;
            }

        }

        ?>

                                appka

                        </span>
                            <div class="ref-inclusion-wrap">


                                <?php
                                $terms = wp_get_post_terms( $post->ID,'type', array(
                                    'orderby'    => 'count',
                                    'hide_empty' => 1
                                ) );



                                foreach ($terms as $term) {


                                    echo '<span class="ref-inclusion">' . $term->name . '</span>';

                                }



                                ?>


                            </div>

                            <?php if(get_field('desktop_mobile_video')=="mobil"){ ?>
                                <img class="ref-img" src="<?php the_field('ilustracni_fotografie') ?>">
                            <?php } ?>

                            <?php if(get_field('desktop_mobile_video')=="desktop"){ ?>
                                <img class="ref-img ref-img--desktop" src="<?php the_field('ilustracni_fotografie') ?>">
                            <?php } ?>
                        </div>
                    </a>
                </div>

            <?php endwhile; ?>





            <div class="ref-button-wrap ref-button-wrap--dev col-xs-12">
                <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-reference.php' ) ) ?>">
                    <button class="ref-button">Více referencí</button>
                </a>
            </div>


        </div>
    </div>










    <div class="cooperation">
        <div class="container subpage container__cooperation">
            <h2>Spolupracujeme s</h2>
            <div class="container-shorter dev-logos clients-wrap-dev">
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/tmobile.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/ct.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/radiocas.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/erabanq.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/dogtrace.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/1.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/ismlouva.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/karlovauni.png"></div>


            </div>
        </div>
    </div>




    <div class="team team__who">

        <div class="container">
            <div class="row">
                <div class="team--text team--text-apps">
                    <div class="col-lg-12 col-md-12">
                        <h2><?php the_field('nadpis_kdo_1', 'options')?> <br><?php the_field('nadpis_kdo_2', 'options')?></h2>
                        <p>
                            <?php the_field('prvni_veta_kdo', 'options')?>
                            <br>
                            <?php the_field('druha_veta_kdo', 'options')?>
                            <br>
                            <?php the_field('treti_veta_kdo', 'options')?>
                        </p>
                    </div>
                </div>

                <!-- MOBIL -->

                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto2.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_prvni', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_prvni', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_prvni', 'options')?></span>
                        </div>
                    </div>
                </div>

                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto1.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_druhy', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_druhy', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_druhy', 'options')?></span>
                        </div>
                    </div>
                </div>
                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto3.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_treti', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_treti', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_treti', 'options')?></span>
                        </div>
                    </div>
                </div>



                <!-- TABLET + DESKTOP -->
                <div class="team--photo">
                </div>


                <div class="team--more__wrap">
                    <span class="more more--first"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--first__text">


                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_prvni', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_prvni', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_prvni', 'options')?></span>
                        </div>


                        <div class="team--text__block">
                        </div>
                    </div>
                </div>



                <div class="team--more__wrap2">
                    <span class="more more--second"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--second__text">
                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_druhy', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_druhy', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_druhy', 'options')?></span>
                        </div>
                        <div class="team--text__block">
                        </div>
                    </div>
                </div>


                <div class="team--more__wrap3">
                    <span class="more more--third"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--third__text">
                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_treti', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_treti', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_treti', 'options')?></span>
                        </div>
                        <div class="team--text__block">
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <?php wp_reset_query(); wp_reset_postdata(); ?>

    <div class="technology-page dev-apps-technology">
        <div class="container subpage">
            <h2><?php the_field('technologie_nadpis_vyvoj_app')?></h2>
            <p><?php the_field('prvni_odstavec_vyvoj_app') ?></p>
            <p><?php the_field('druhy_odstavec_vyvoj_app') ?></p>
            <p><?php the_field('treti_odstavec_vyvoj_app') ?></p>
            <div class="container-shorter dev-logos">

                <?php if(get_field('java_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/1.png"></div>
                <?php } ?>
                <?php if(get_field('laravel_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/2.png"></div>
                <?php } ?>
                <?php if(get_field('swift_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/3.png"></div>
                <?php } ?>
                <?php if(get_field('gopay_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/4.png"></div>
                <?php } ?>
                <?php if(get_field('php_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/5.png"></div>
                <?php } ?>
                <?php if(get_field('vyfakturuj_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/6.png"></div>
                <?php } ?>
                <?php if(get_field('react_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/7.png"></div>
                <?php } ?>
                <?php if(get_field('firebase_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/8.png"></div>
                <?php } ?>
                <?php if(get_field('retrofit_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/9.png"></div>
                <?php } ?>
                <?php if(get_field('crashlytics_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/10.png"></div>
                <?php } ?>
                <?php if(get_field('realm_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/11.png"></div>
                <?php } ?>
                <?php if(get_field('googlean_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/12.png"></div>
                <?php } ?>
                <?php if(get_field('mysql_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/13.png"></div>
                <?php } ?>
                <?php if(get_field('oauth_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/14.png"></div>
                <?php } ?>
                <?php if(get_field('ios_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/15.png"></div>
                <?php } ?>
                <?php if(get_field('android_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/16.png"></div>
                <?php } ?>
                <?php if(get_field('kotlin_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/kotlin.png"></div>
                <?php } ?>
                <?php if(get_field('node_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/node.png"></div>
                <?php } ?>
                <?php if(get_field('vuejs_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/vuejs.png"></div>
                <?php } ?>
                <?php if(get_field('amazon_tech')) { ?>
                    <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/amazon.png"></div>
                <?php } ?>
            </div>
        </div>
    </div>



    <div class="what-next__wrap-other">
        <?php get_template_part('parts/category', 'what-next') ?>

    </div>





    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>


<?php endwhile; ?>
<?php get_footer(); ?>