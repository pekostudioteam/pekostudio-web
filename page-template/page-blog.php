<?php /* Template name: Blog*/ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php
    if (get_field('align2', 'options')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>

<header class="secondary-header blog-bg "   style="background: url(<?php the_field('blog_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
    <div class="header-shaddow">

        <div class="row content">
            <?php get_template_part('parts/category', 'menu') ?>
        </div>

        <div class="row content">
            <h1><?php the_title() ?></h1>
        </div>

    </div>
</header>



    <div class="container subpage blog-page">
        <div class="row">

            <h2>Nejnovější články</h2>
            <div class="top-post-white"></div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <div class="top-post-gray"></div>
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <div class="top-post-gray"></div>
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto2.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <div class="top-post-gray top-post-gray-tablet"></div>
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto3.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto2.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto3.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto2.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="/single-blog.php">
                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php echo get_stylesheet_directory_uri() ?>/images/blog-foto3.png">
                    <span class="article-title">Jak urychlit programování v PHP?<br>10 užitečných tipů</span>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. </p>

                    <div class="blog--card__date">
                        <p>2.3.2018</p>
                    </div>

                    <div class="blog--card__signature">
                        <p>Petr Kott</p>
                    </div>
                </a>
            </div>


            <button class="blog-button">Starší články</button>
        </div>
    </div>



    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>




<?php endwhile; ?>
<?php get_footer(); ?>