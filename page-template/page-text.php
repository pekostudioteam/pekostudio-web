<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>




    <div class="container blog-detail-page">
        <div class="row content">
            <h1><?php the_title() ?></h1>
        </div>
        <div class="row">

            <div class="col-xs-12 blog-detail-content">

                <?php the_content()?>

            </div>


        </div>
    </div>




    <div class="what-next__wrap-other">
        <?php get_template_part('parts/category', 'what-next') ?>

    </div>





    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>



    <?php get_footer(); ?>
<?php endwhile; ?>