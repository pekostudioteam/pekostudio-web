<?php /* Template name: Kontakt */ ?>
<?php get_header(); ?>
<?php if (have_posts()) while (have_posts()) : the_post(); ?>


    <?php
    if (get_field('header_align') == "left") {
        $align = "left";
    } else {
        $align = "right";
    }
    ?>

    <header class="secondary-header contact-bg"
            style="background: url(<?php the_field('header_photo') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1><?php the_title() ?></h1>
            </div>

        </div>
    </header>


    <div class="what-next__wrap-contact">
        <div class="container subpage contact-h2">
            <h2>Napište nám</h2>
        </div>

        <?php get_template_part('parts/category', 'what-next') ?>
    </div>


    <footer class="footer what-next__wrap-footer">

        <div class="container">
            <div class="row">
                <h3>Kontakt</h3>

                <div class="col-lg-5 col-md-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-6">
                            <div class="contact-info">
                                <a href="tel:<?php the_field('telefon', 'options') ?>"><span
                                            class="contact_svg contact_svg--tel"><?php get_template_part('svg/ico', 'tel') ?></span><span
                                            class="footer--phone"><?php the_field('telefon', 'options') ?></span></a><br>
                                <a href="mailto:<?php the_field('email', 'options') ?>"><span
                                            class="contact_svg contact_svg--mail"><?php get_template_part('svg/ico', 'mail') ?></span><span
                                            class="footer--email"><?php the_field('email', 'options') ?></span></a><br>
                                <!-- fix pointer -->
                                <div onclick="mapsSelector()" style="cursor:pointer;">
                                    <span onclick="mapsSelector()"
                                          class="contact_svg contact_svg--navi"><?php get_template_part('svg/ico', 'navigate') ?></span><span
                                            onclick="mapsSelector()" class="footer--navi">Kde nás najdete?</span>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-6">
                            <div class="contact-text contact-text__first">
                                <p>
                                    <b><?php the_field('nazev_firmy', 'options') ?></b><br>
                                    <?php the_field('ulice', 'options') ?><br>
                                    <?php the_field('mesto_a_psc', 'options') ?><br>

                                </p>
                            </div>


                            <div class="contact-text">
                                <p>
                                    IČ: <?php the_field('ic', 'options') ?><br>
                                    DIČ: <?php the_field('dic', 'options') ?>
                                </p>
                            </div>
                            <div class="contact-text contact-text__last">
                                <p>
                                    Č.ú.: <?php the_field('cislo_uctu', 'options') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <div id="map" class="map__contact"></div>


    </footer>

<?php endwhile; ?>
<?php get_footer(); ?>