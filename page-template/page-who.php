<?php /* Template name: Who */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <?php
    if (get_field('header_align')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>
    <header class="secondary-header who-bg"  style="background: url(<?php the_field('header_photo') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1><?php the_title() ?></h1>
            </div>

        </div>
    </header>


    <div class="container subpage who-page">
        <div class="container-shorter">
            <h2>
                <?php the_field('nadpis') ?>
            </h2>
            <p>
                <?php the_field('prvni_odstavec') ?>
            </p>
            <p>
                <?php the_field('druhy_odstavec') ?>
            </p>
            <ul class="ul-margin">

                <?php while ( have_rows('odrazky') ) : the_row(); ?>
                    <li><?php the_sub_field('odrazka')?></li>
                <?php endwhile; ?>

            </ul>
        </div>

    </div>


    <div class="container-larger container-larger__reduce subpage who-page">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <img class="first-who-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/foto26.jpg">
            </div>
            <div class="col-md-6 col-sm-6">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto15.jpg">
            </div>
        </div>
    </div>

    <div class="container-larger subpage who-page">
        <div class="row">

            <div class="col-lg-6">
                <div class="who-svg who-svg__orange">
                    <?php get_template_part('svg/ico', 'projects') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_1') ?></h2>
                    <p><?php the_field('text_top_1') ?></p>
                </div>

                <div class="who-svg who-svg__orange">
                    <?php get_template_part('svg/ico', 'experience') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_2') ?></h2>
                    <p><?php the_field('text_top_2') ?></p>
                </div>

                <div class="who-svg who-svg__dark-orange">
                    <?php get_template_part('svg/ico', 'startup') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_3') ?></h2>
                    <p><?php the_field('text_top_3') ?></p>
                </div>

                <div class="who-svg who-svg__red">
                    <?php get_template_part('svg/ico', 'product') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_4') ?></h2>
                    <p><?php the_field('text_top_4') ?></p>
                </div>

            </div>

            <div class="col-lg-6">

                <div class="who-svg who-svg__orange">
                    <?php get_template_part('svg/ico', 'speed') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_5') ?></h2>
                    <p><?php the_field('text_top_5') ?></p>
                </div>

                <div class="who-svg who-svg__dark-orange">
                    <?php get_template_part('svg/ico', 'partnership') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_6') ?></h2>
                    <p><?php the_field('text_top_6') ?></p>
                </div>

                <div class="who-svg who-svg__red">
                    <?php get_template_part('svg/ico', 'open') ?>
                </div>
                <div class="who-positives">
                    <h2><?php the_field('nadpis_top_7') ?></h2>
                    <p><?php the_field('text_top_7') ?></p>
                </div>
            </div>

        </div>
    </div>





    <div class="who-what-wrap who-page">
        <div class="who-what-wrap-gray"></div>
        <div class="container-larger subpage who-page">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-5">

                    <?php if(get_field('prvni_radek')){ ?>
                        <div>
                            <span class="check-svg"><?php get_template_part('svg/ico', 'check') ?></span>
                            <h2 class="what-h2"><?php the_field('prvni_radek') ?></h2>
                        </div>
                    <?php } ?>

                    <?php if(get_field('druhy_radek')){ ?>
                        <div>
                            <span class="check-svg"><?php get_template_part('svg/ico', 'check') ?></span>
                            <h2 class="what-h2"><?php the_field('druhy_radek') ?></h2>
                        </div>
                    <?php } ?>

                    <?php if(get_field('treti_radek')){ ?>
                        <div>
                            <span class="check-svg"><?php get_template_part('svg/ico', 'check') ?></span>
                            <h2 class="what-h2"><?php the_field('treti_radek') ?></h2>
                        </div>
                    <?php } ?>

                </div>


                <div class="col-sm-12 absolute-images-who">
                    <img class="who-what-img who-what-img-first" src="<?php echo get_stylesheet_directory_uri() ?>/images/foto8.jpg">
                    <img class="who-what-img who-what-img-second" src="<?php echo get_stylesheet_directory_uri() ?>/images/foto32.jpg">

                </div>
            </div>

        </div>
    </div>







    <div class="team team__who">

        <div class="container">
            <div class="row">
                <div class="team--text team--text-who">
                    <div class="col-lg-12 col-md-12">
                        <h2><?php the_field('nadpis_kdo_1', 'options')?> <br><?php the_field('nadpis_kdo_2', 'options')?></h2>
                        <p>
                            <?php the_field('prvni_veta_kdo', 'options')?>
                            <br>
                            <?php the_field('druha_veta_kdo', 'options')?>
                            <br>
                            <?php the_field('treti_veta_kdo', 'options')?>
                        </p>
                    </div>
                </div>

                <!-- MOBIL -->

                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto2.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_prvni', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_prvni', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_prvni', 'options')?></span>
                        </div>
                    </div>
                </div>

                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto1.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_druhy', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_druhy', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_druhy', 'options')?></span>
                        </div>
                    </div>
                </div>
                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto3.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_treti', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_treti', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_treti', 'options')?></span>
                        </div>
                    </div>
                </div>



                <!-- TABLET + DESKTOP -->
                <div class="team--photo">
                </div>


                <div class="team--more__wrap">
                    <span class="more more--first"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--first__text">


                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_prvni', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_prvni', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_prvni', 'options')?></span>
                        </div>


                        <div class="team--text__block">
                        </div>
                    </div>
                </div>



                <div class="team--more__wrap2">
                    <span class="more more--second"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--second__text">
                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_druhy', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_druhy', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_druhy', 'options')?></span>
                        </div>
                        <div class="team--text__block">
                        </div>
                    </div>
                </div>


                <div class="team--more__wrap3">
                    <span class="more more--third"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--third__text">
                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_treti', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_treti', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_treti', 'options')?></span>
                        </div>
                        <div class="team--text__block">
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>






    <div class="what-next__wrap-other what-next__wrap-other-who">
        <?php get_template_part('parts/category', 'what-next') ?>
    </div>





    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>


<?php endwhile; ?>
<?php get_footer(); ?>