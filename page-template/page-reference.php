<?php /* Template name: Reference */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <?php
    if (get_field('align4', 'options')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>
    <header class="secondary-header ref-bg"  style="background: url(<?php the_field('ref_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1><?php the_title() ?></h1>
            </div>

        </div>
    </header>






    <div class="container subpage reduce-pad-bot reference-page">
        <?php
        $terms = get_terms( 'platforms', array(
            'orderby'    => 'count',
            'hide_empty' => 0
        ) );



        echo '<div class="filter-wrap">';
        echo '<span class="filter filter-title">Platforma:</span>';


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {
            $current_term = is_tax ? get_queried_object() : null;
            $classactive = "";
            if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
                $classactive = "active-filter";
            }

            if ($i == $len - 1) {
                echo '<span class="filter  filter-main filter-main-last ' . $classactive . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
            }
            else {
                echo '<span class="filter  filter-main ' . $classactive . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
            }
            $i++;
        }


        echo '</div>';
        ?>

    </div>


    <div class="container-larger reduce-pad-bot subpage reference-page">
        <div class="row child-ref" id="refresh">




            <?php
            $pageItems = $_GET["query"] ? $_GET["query"] : 9; //9 * $paged;
            $paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
            $args = array( 'post_type' => 'reference', 'posts_per_page' => $pageItems);
            $loop = new WP_Query( $args );
            ?>

            <?php  while ( $loop->have_posts() ) :$loop->the_post();
                global $post;

                ?>



                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                    <a href="<?php the_permalink()?>">


                        <div class="ref-card">





                            <h2><?php the_field('nadpis_reference') ?></h2>

                            <span class="ref-what">


        <?php
        $terms = wp_get_post_terms( $post->ID,'platforms', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {


            if ($len > 1){
                if ($len > 2){
                    if ($i == 0) {
                        echo $term->name . ', ';
                    }
                    else if ($i == $len - 1) {
                        echo ' a ' . $term->name;
                    }

                    else {
                        echo $term->name;
                    }
                    $i++;
                }
                else {
                    if ($i == $len - 1) {
                        echo ' a ' . $term->name;
                    }
                    else {
                        echo $term->name;
                    }
                    $i++;

                }
            }



            else {
                echo $term->name;
            }

        }

        ?>

                                apka

                        </span>
                            <div class="ref-inclusion-wrap">


                                <?php
                                $terms = wp_get_post_terms( $post->ID,'type', array(
                                    'orderby'    => 'count',
                                    'hide_empty' => 1
                                ) );



                                foreach ($terms as $term) {


                                    echo '<span class="ref-inclusion">' . $term->name . '</span>';

                                }



                                ?>


                            </div>

                            <?php if(get_field('desktop_mobile_video')=="mobil"){ ?>
                                <img class="ref-img" src="<?php the_field('ilustracni_fotografie') ?>">
                            <?php } ?>

                            <?php if(get_field('desktop_mobile_video')=="desktop"){ ?>
                                <img class="ref-img ref-img--desktop" src="<?php the_field('ilustracni_fotografie') ?>">
                            <?php } ?>
                        </div>
                    </a>
                </div>

            <?php endwhile; ?>




            <?php if( $paged >= 1 ): ?>
            <div class="ref-button-wrap col-xs-12">
                    <?php /* previous_posts_link('<button class="ref-button">Předchozí reference</button>'); */?>

                    <!-- <?php next_posts_link('<button class="ref-button" id="more_refs">Další reference</button>', $loop->max_num_pages); ?> -->

                    <?php 
                    $argss = array( 'post_type' => 'reference');

                    $query = new WP_Query( $argss );
                    $total = $query->found_posts;

                    if($total >= intval($_GET["query"]) || !$_GET["query"]){
                        echo '<button class="ref-button" id="more_refs">Další reference</button>';
                    }else{
                        echo "";
                    }
                    ?>
            </div>
            <?php  endif; ?>








        </div>
    </div>

    <div class="cooperation cooperation--reference">
        <div class="container subpage container__cooperation">
            <h2>Mezi naše klienty patří například</h2>
            <div class="container-shorter dev-logos">
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/tmobile.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/ct.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/radiocas.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/erabanq.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/dogtrace.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/1.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/ismlouva.png"></div>
                <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/karlovauni.png"></div>
            </div>
        </div>
    </div>



    <div class="what-next__wrap-other">
        <?php get_template_part('parts/category', 'what-next') ?>

    </div>





    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>


<?php endwhile; ?>
<?php get_footer(); ?>