<?php /* Template name: Thanks */ ?>

    <!-- Event snippet for Odeslání formuláře pro zájemce conversion page -->
    <script>
        gtag('event', 'conversion', {'send_to': 'AW-817153571/i03MCLy2itEBEKOM04UD'});
    </script>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>



    <?php
    if (get_field('header_align')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>

    <header class="secondary-header contact-bg" style="background: url(<?php the_field('header_photo') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1>Děkujeme za kontaktování</h1>
            </div>

        </div>
    </header>





    <div class="what-next__wrap-thanks">
        <div class="container subpage thanks-h2">
            <h2>V nejbližší době Vám odpovíme</h2>
        </div>
        <div class="container container-shorter">
            <div class="thanks-message">
               <!-- <p class="other_p">Děkujeme Vám za Váš e-mail, hned jak to bude možné, tak Vám odpovíme.</p>-->
            </div>
        </div>



    </div>


    <div class="blog">
        <div class="container">

            <div class="row child-hp-blog">
                <h3>Blog</h3>



                <?php

                $args = array(
                    'post_type'=> 'post',
                    'orderby'    => 'ID',
                    'post_status' => 'publish',
                    'order'    => 'DESC',
                    'posts_per_page' => 3 // this will retrive all the post that is published
                );
                $result = new WP_Query( $args );
                if ( $result-> have_posts() ) : ?>
                    <?php while ( $result->have_posts() ) : $result->the_post(); ?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12  blog--card">
                            <a href="<?php the_permalink() ?>">
                                <div class="blog-img-wrap">
                                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php the_field('fotografie')?>">
                                </div>
                                <div class="blog-title-wrap">
                                    <span class="article-title">
                                        <?php the_field('nadpis_clanku')?>
                                        <br>
                                        <?php the_field('podnadpis_clanku')?>
                                    </span>
                                </div>



                                <p class="blog--p">
                                    <?php

                                    $maxLength = 110;
                                    $nazev = substr(get_field('prvni_odstavec'), 0, $maxLength);
                                    echo "$nazev..";

                                    ?>
                                </p>

                                <div class="blog--card__date">
                                    <p><?php the_field('datum')?></p>
                                </div>

                                <div class="blog--card__signature">
                                    <p><?php the_field('autor_clanku')?></p>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>


                <div class="col-xs-12">
                    <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-blog.php' ) ) ?>"><button class="hp--blog-button">Více článků</button></a>
                </div>

            </div>
        </div>
    </div>




    <footer class="footer what-next__wrap-footer">

        <div class="container">
            <div class="row">
                <h3>Kontakt</h3>

                <div class="col-lg-5 col-md-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-6">
                            <div class="contact-info">
                                <a href="tel:<?php the_field('telefon', 'options') ?>"><span class="contact_svg contact_svg--tel"><?php get_template_part('svg/ico', 'tel') ?></span><span class="footer--phone"><?php the_field('telefon', 'options') ?></span></a><br>
                                <a href="mailto:<?php the_field('email', 'options') ?>"><span class="contact_svg contact_svg--mail"><?php get_template_part('svg/ico', 'mail') ?></span><span class="footer--email"><?php the_field('email', 'options') ?></span></a><br>
                                <!-- fix pointer -->
                                <div onclick="mapsSelector()" style="cursor:pointer;">
                                    <span onclick="mapsSelector()" class="contact_svg contact_svg--navi"><?php get_template_part('svg/ico', 'navigate') ?></span><span onclick="mapsSelector()" class="footer--navi">Kde nás najdete?</span>
                                </div>
                            
                            
                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-6">
                            <div class="contact-text contact-text__first">
                                <p>
                                    <b><?php the_field('nazev_firmy', 'options') ?></b><br>
                                    <?php the_field('ulice', 'options') ?><br>
                                    <?php the_field('mesto_a_psc', 'options') ?><br>

                                </p>
                            </div>




                            <div class="contact-text">
                                <p>
                                    IČ: <?php the_field('ic', 'options') ?><br>
                                    DIČ: <?php the_field('dic', 'options') ?>
                                </p>
                            </div>
                            <div class="contact-text contact-text__last">
                                <p>
                                    Č.ú.: <?php the_field('cislo_uctu', 'options') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <div id="map" class="map__contact"></div>



    </footer>

<?php endwhile; ?>
    <!-- Měřicí kód Sklik.cz -->
    <script type="text/javascript">
        var seznam_cId = 100064056;
        var seznam_value = null;
    </script>
    <script type="text/javascript" src="https://www.seznam.cz/rs/static/rc.js" async></script>
<?php get_footer(); ?>