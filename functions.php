<?php


// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 830;

add_action( 'after_setup_theme', 'peko_setup' );
if ( ! function_exists( 'peko_setup' ) ):
    function peko_setup() {

        // This theme styles the visual editor with editor-style.css to match the theme style.
        add_editor_style( 'dist/editor-style.css' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'primary'   => __( 'Menu', 'peko' )
        ) );

        add_theme_support( 'html5', array( 'gallery', 'caption', 'search-form' ) );
        add_theme_support( 'title-tag' );
        add_editor_style( 'dist/editor-style.css' );
        add_theme_support( 'post-thumbnails', array( 'post' ) );

        add_image_size('small_gallery', 164, 133, true);
        add_image_size('ico', 100, 100, true);
        add_image_size('banner_responsive', 991, 300, true);
        add_image_size('contact_img', 412, 266, true);
        add_image_size('product_img', 500, 500, true);
        add_image_size('square_bg', 768, 768, true);
    }
endif;







if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Šablona',
        'menu_title'	=> 'Šablona',
        'menu_slug' 	=> 'info',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Zápatí',
        'menu_title'	=> 'Zápatí',
        'parent_slug'	=> 'info'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Kontaktní informace',
        'menu_title'	=> 'Kontaktní informace',
        'parent_slug'	=> 'info'
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Co dále',
        'menu_title'	=> 'Co dále',
        'parent_slug'	=> 'info'
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Kdo vlastně jsme',
        'menu_title'	=> 'Kdo vlastně jsme',
        'parent_slug'	=> 'info'
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Technologie',
        'menu_title'	=> 'Technologie',
        'parent_slug'	=> 'info'
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Fotky hlavičky',
        'menu_title'	=> 'Fotky hlavičky',
        'parent_slug'	=> 'info'
    ));
}



add_action( 'init', 'cpt_reference' );
add_action( 'init', 'tax_platform_category' );
add_action( 'init', 'tax_type_category' );


function cpt_reference() {
    $labels = array(
        'name'                => _x( 'Reference', 'Post Type General Name', 'peko' ),
        'singular_name'       => _x( 'Reference', 'Post Type Singular Name', 'peko' ),
        'menu_name'           => __( 'Reference', 'peko' ),
        'name_admin_bar'      => __( 'Reference', 'peko' ),
        'parent_item_colon'   => __( 'Nadřazené reference:', 'peko' ),
        'all_items'           => __( 'Všechny reference', 'peko' ),
        'add_new_item'        => __( 'Přidat novou referenci', 'peko' ),
        'add_new'             => __( 'Přidat novou referenci', 'peko' ),
        'new_item'            => __( 'Nová reference', 'peko' ),
        'edit_item'           => __( 'Upravit referenci', 'peko' ),
        'update_item'         => __( 'Nahrát referenci', 'peko' ),
        'view_item'           => __( 'Zobrazit referenci', 'peko' ),
        'search_items'        => __( 'Hledat reference', 'peko' ),
        'not_found'           => __( 'Nenalezeno', 'peko' ),
        'not_found_in_trash'  => __( 'Nenalezeno v koši', 'peko' ),
    );

    $args = array(
        'label'               => __( 'Reference', 'peko' ),
        'description'         => __( 'Reference', 'peko' ),
        'labels'              => $labels,
        'supports'            => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'           => array('slug' => 'reference')
    );
    register_post_type( 'reference', $args );
}



function tax_platform_category() {

    $labels = array(
        'name'              => _x( 'Platforma', 'Platforma' ),
        'singular_name'     => _x( 'Platforma', 'Platforma' ),
        'search_items'      => __( 'Vyhledat platformu' ),
        'all_items'         => __( 'Všechny platformy' ),
        'parent_item'       => __( 'Nadřazená platforma' ),
        'parent_item_colon' => __( 'Nadřazené platformy:' ),
        'edit_item'         => __( 'Editovat platformu' ),
        'update_item'       => __( 'Aktualizovat platformu' ),
        'add_new_item'      => __( 'Přidat novou platformu' ),
        'new_item_name'     => __( 'Nová platforma' ),
        'menu_name'         => __( 'Platforma' )
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'platforma' )
    );

    register_taxonomy( 'platforms', ['reference'], $args );
}



add_action( 'init', 'my_custom_page_word' );

function my_custom_page_word() {
    global $wp_rewrite;  // Get the global wordpress rewrite-rules/settings

    // Change the base pagination property which sets the wordpress pagination slug.
    $wp_rewrite->pagination_base = "stranka";  //where new-slug is the slug you want to use ;)
}



function tax_type_category() {

    $labels = array(
        'name'              => _x( 'Typ', 'Typ' ),
        'singular_name'     => _x( 'Typ', 'Typ' ),
        'search_items'      => __( 'Vyhledat typ' ),
        'all_items'         => __( 'Všechny typy' ),
        'parent_item'       => __( 'Nadřazený typ' ),
        'parent_item_colon' => __( 'Nadřazené typy:' ),
        'edit_item'         => __( 'Editovat typ' ),
        'update_item'       => __( 'Aktualizovat typ' ),
        'add_new_item'      => __( 'Přidat nový typ' ),
        'new_item_name'     => __( 'Nová typ' ),
        'menu_name'         => __( 'Typ' )
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'typ' )
    );

    register_taxonomy( 'type', ['reference'], $args );
}





function enqueue_scripts_styles_init() {

    $translation = array(
        'ajax_url' => admin_url('admin-ajax.php')
    );
    wp_enqueue_script( 'peko-script', get_stylesheet_directory_uri() . '/js/script.js', array(), NULL, TRUE );


     // add

     wp_localize_script( 'peko-script', 'my_ajax_object',
     array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
// /


    wp_enqueue_script( 'peko-script-slick', get_stylesheet_directory_uri() . '/modules/slick/slick.min.js', array(), NULL, FALSE );

    wp_enqueue_script( 'peko-script-map', get_stylesheet_directory_uri() . '/js/map.js', array(), NULL, FALSE );



    wp_enqueue_script( 'peko-script-tr' );
    wp_localize_script( 'peko-script-tr-tr', 'object', $translation );


    wp_enqueue_style( 'font-sans', 'https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900', array(), NULL );

    wp_enqueue_style( 'peko', get_stylesheet_directory_uri() . '/css/style.css', array(), 20180508 );


    wp_enqueue_style( 'peko-slick', get_stylesheet_directory_uri() . '/modules/slick/slick.css', array(), NULL );

    wp_enqueue_style( 'peko-slick-theme', get_stylesheet_directory_uri() . '/modules/slick/slick-theme.css', array(), NULL );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_styles_init' );




function getPageIDByTemplate($template)
{
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => $template
    ));

    if (!empty($pages)) {
        return $pages[0]->ID;
    }
}







function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyCjHsSALqSFj8Wle0NSvx0YCggt01gP_iY');
}

add_action('acf/init', 'my_acf_init');


function add_lazy_load(){
    wp_enqueue_style("lazy-load", "https://unpkg.com/aos@2.3.1/dist/aos.css", array(), "all" );
    wp_register_script('lazy-load', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array(), '1.1', false);
    wp_enqueue_script('lazy-load');

}
add_action( 'wp_enqueue_scripts', 'add_lazy_load'); 

// load more


function my_enqueue() {

    //wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );

    wp_localize_script( 'ajax-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue' );




add_action( 'wp_footer', 'mycustom_wp_footer' );

function mycustom_wp_footer() {
    ?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            location.href = '/dekujeme'
        }, false );
    </script>
    <?php
}



function more_post_ajax(){
    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 9;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    header("Content-Type: text/html");

    $args = array(
        'post_type' => 'reference',
        'posts_per_page' => $ppp,
        'paged'    => $page,
    );

    //$paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
    //$args = array( 'post_type' => 'reference', 'posts_per_page' => 9, 'paged' => $paged);
    //$loop = new WP_Query( $args );


    $loop = new WP_Query($args);

    $out = '';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
    global $post;
       /* $out .= '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                <a href="'.the_permalink().'">
                
                <div class="ref-card">

                <h2>'.the_field("nadpis_reference").'</h2>     
                <span class="ref-what">   
                </div>
         </div>'; */



         $link = get_permalink();
        $title = get_field("nadpis_reference");
        
        $image = get_field("desktop_mobile_video");
        if($image == "mobil"){
            
        }


         $headerHeredoc .= <<<EOT
                   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                    <a href="$link">
                        <div class="ref-card">
                            <h2>$title</h2>
                            <span class="ref-what">
                         
        EOT;
        $terms = wp_get_post_terms( $post->ID,'platforms', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {


            if ($len > 1){
                if ($len > 2){
                    if ($i == 0) {
                        //echo $term->name . ', ';
                        $headerHeredoc .= <<<EOT
                            $term->name, 
                        EOT;
                    }
                    else if ($i == $len - 1) {
                        //echo ' a ' . $term->name;
                        $headerHeredoc .= <<<EOT
                            $term->name a 
                        EOT;
                    }

                    else {
                        //echo $term->name;
                        $headerHeredoc .= <<<EOT
                            $term->name 
                        EOT;
                    }
                    $i++;
                }
                else {
                    if ($i == $len - 1) {
                        //echo ' a ' . $term->name;
                        $headerHeredoc .= <<<EOT
                           a $term->name 
                        EOT;
                    }
                    else {
                        //echo $term->name;
                        $headerHeredoc .= <<<EOT
                            $term->name 
                        EOT;
                    }
                    $i++;

                }
            }



            else {
                //echo $term->name;
                $headerHeredoc .= <<<EOT
                    $term->name

                EOT;
            }

        }

        $headerHeredoc .= <<<EOT
            appka</span>
            <div class="ref-inclusion-wrap">

            EOT;

        $terms = wp_get_post_terms( $post->ID,'type', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );



        foreach ($terms as $term) {


           // echo '<span class="ref-inclusion">' . $term->name . '</span>';
           $headerHeredoc .= <<<EOT
                <span class="ref-inclusion">$term->name</span>
           EOT;
        }


        if($image == "mobil"){
            $ilustration_photo = get_field("ilustracni_fotografie");
            $headerHeredoc .= <<<EOT
            </div>
            <img class="ref-img" src="$ilustration_photo">
  
            EOT;
        }
        if($image == "desktop"){
            $ilustration_photo = get_field("ilustracni_fotografie");
            $headerHeredoc .= <<<EOT
            </div>
            <img class="ref-img ref-img--desktop" src="$ilustration_photo">
  
            EOT;
        }

        $headerHeredoc .= <<<EOT

                </div>
                </a>
            </div>
        EOT;
        //

        ///

    endwhile;
    endif;
    wp_reset_postdata();
    //die($out);
    die($headerHeredoc);
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');



function total_posts_ajax(){
    $argss = array( 'post_type' => 'reference');

    $query = new WP_Query( $argss );
    $total = $query->found_posts;

    echo $total;
    wp_die();
}


add_action('wp_ajax_nopriv_total_posts_ajax', 'total_posts_ajax');
add_action('wp_ajax_total_posts_ajax', 'total_posts_ajax');




// platforms 
function more_post_platform(){
    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 9;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
    $platform_type = $_POST["platformType"];

    header("Content-Type: text/html");

    $args = array(
        'post_type' => 'reference',
        'posts_per_page' => $ppp,
        'paged'    => $page,
        'tax_query' => array(
            [
                'taxonomy' => 'platforms',
                'field' => 'slug',
                'terms' => $platform_type,
            ]
    ));

    $loop = new WP_Query($args);

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
    global $post;

         $link = get_permalink();
        $title = get_field("nadpis_reference");
        
        $image = get_field("desktop_mobile_video");
        if($image == "mobil"){
            
        }


         $headerHeredoc .= <<<EOT
                   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 reference">
                    <a href="$link">
                        <div class="ref-card">
                            <h2>$title</h2>
                            <span class="ref-what">
                         
        EOT;
        $terms = wp_get_post_terms( $post->ID,'platforms', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {


            if ($len > 1){
                if ($len > 2){
                    if ($i == 0) {
                        //echo $term->name . ', ';
                        $headerHeredoc .= <<<EOT
                            $term->name, 
                        EOT;
                    }
                    else if ($i == $len - 1) {
                        //echo ' a ' . $term->name;
                        $headerHeredoc .= <<<EOT
                            $term->name a 
                        EOT;
                    }

                    else {
                        //echo $term->name;
                        $headerHeredoc .= <<<EOT
                            $term->name 
                        EOT;
                    }
                    $i++;
                }
                else {
                    if ($i == $len - 1) {
                        //echo ' a ' . $term->name;
                        $headerHeredoc .= <<<EOT
                           a $term->name 
                        EOT;
                    }
                    else {
                        //echo $term->name;
                        $headerHeredoc .= <<<EOT
                            $term->name 
                        EOT;
                    }
                    $i++;

                }
            }



            else {
                //echo $term->name;
                $headerHeredoc .= <<<EOT
                    $term->name

                EOT;
            }

        }

        $headerHeredoc .= <<<EOT
            appka</span>
            <div class="ref-inclusion-wrap">

            EOT;

        $terms = wp_get_post_terms( $post->ID,'type', array(
            'orderby'    => 'count',
            'hide_empty' => 1
        ) );



        foreach ($terms as $term) {


           // echo '<span class="ref-inclusion">' . $term->name . '</span>';
           $headerHeredoc .= <<<EOT
                <span class="ref-inclusion">$term->name</span>
           EOT;
        }


        if($image == "mobil"){
            $ilustration_photo = get_field("ilustracni_fotografie");
            $headerHeredoc .= <<<EOT
            </div>
            <img class="ref-img" src="$ilustration_photo">
  
            EOT;
        }
        if($image == "desktop"){
            $ilustration_photo = get_field("ilustracni_fotografie");
            $headerHeredoc .= <<<EOT
            </div>
            <img class="ref-img ref-img--desktop" src="$ilustration_photo">
  
            EOT;
        }

        $headerHeredoc .= <<<EOT

                </div>
                </a>
            </div>
        EOT;
        //

        ///

    endwhile;
    endif;
    wp_reset_postdata();
    //die($out);
    die($headerHeredoc);
}

add_action('wp_ajax_nopriv_more_post_platform', 'more_post_platform');
add_action('wp_ajax_more_post_platform', 'more_post_platform');



function platform_total_posts_ajax(){
    $platform_type = $_POST["platformType"];
    $args_platform = array(
        'post_type' => 'reference',
        'tax_query' => array(
            [
                'taxonomy' => 'platforms',
                'field' => 'slug',
                'terms' => $platform_type,
            ]
    ));
    $query = new WP_Query( $args_platform );
    $total = $query->found_posts;

    echo $total;
    wp_die();
}


add_action('wp_ajax_nopriv_platform_total_posts_ajax', 'platform_total_posts_ajax');
add_action('wp_ajax_platform_total_posts_ajax', 'platform_total_posts_ajax');


