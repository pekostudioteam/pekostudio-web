<?php /* Template name: Blog*/ ?>
<?php get_header(); ?>


<?php
if (get_field('align2', 'options')=="left"){
    $align="left";
}
else{
    $align="right";
}
?>

    <header class="secondary-header blog-bg "   style="background: url(<?php the_field('blog_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1>Blog</h1>
            </div>

        </div>
    </header>

    <div class="container subpage reduce-pad-bot reference-page">
        <?php



        $terms = get_categories();



        echo '<div class="filter-wrap filter-wrap-platform">';
        echo '<span class="filter filter-title">Kategorie:</span>';


        $i = 0;
        $len = count($terms);
        foreach ($terms as $term) {

          //  $current_term = $terms[0];
            $classactive = "";
        //    if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
        //        $classactive = "active-filter";
        //    }


            if ($i == $len - 1) {
                echo '<span class="filter filter-main filter-main-last ' . $classactive . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
            }
            else {
                echo '<span class="filter filter-main ' . $classactive . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></span>';
            }
            $i++;
        }


        echo '</div>';
        ?>

    </div>


    <div class="container subpage blog-page padd-0">
        <div class="row child-top-blog">

            <h2>Nejnovější články</h2>
            <div class="top-post-white"></div>

<?php
$top = get_field( "top_clanek" );

$paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
$args = array( 'posts_per_page' => 3,'paged' => $paged,
    'meta_query'	=> array(
        'relation'		=> 'OR',
        array(
            'key'		=> 'top_clanek',
            'value'		=>  $top,
            'compare'	=> 'LIKE'
        )
    ));

$firstloop = new WP_Query( $args );

$exclude_posts = [];
?>



<?php  while ( $firstloop->have_posts() ) :$firstloop->the_post();
    $pagedTest = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
    global $post;

    $exclude_posts[] = $post->ID;

    ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card-dark blog--card ">
                <?php if($pagedTest == 1){ ?>
                    <div class="top-post-gray"></div>

                    <a href="<?php the_permalink() ?>">
                    <div class="blog-img-wrap">
                        <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php the_field('fotografie')?>">
                    </div>
                    <div class="blog-title-wrap">
                        <span class="article-title">
                            <?php the_field('nadpis_clanku')?>
                            <br>
                            <?php the_field('podnadpis_clanku')?>
                        </span>
                    </div>
                    <p class="blog--p">
                        <?php

                        $maxLength = 110;
                        $nazev = substr(get_field('prvni_odstavec'), 0, $maxLength);
                        echo "$nazev..";

                        ?>
                    </p>

                    <div class="blog--card__date">
                        <p><?php the_field('datum')?></p>
                    </div>

                    <div class="blog--card__signature">
                        <p><?php the_field('autor_clanku')?></p>
                    </div>
                </a>
                <?php } ?>
            </div>

<?php endwhile; ?>

            <?php wp_reset_query(); wp_reset_postdata(); ?>


            <?php
            $paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
            $args = array( 'posts_per_page' => 9, 'paged' => $paged,'post__not_in' => $exclude_posts);
            $loop = new WP_Query( $args );
            ?>

            <?php  while ( $loop->have_posts() ) :$loop->the_post();
                global $post;

                ?>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 blog--card">
                <a href="<?php the_permalink() ?>">
                    <div class="blog-img-wrap">
                        <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php the_field('fotografie')?>">
                    </div>
                    <div class="blog-title-wrap">
                     <span class="article-title"><?php the_field('nadpis_clanku')?><br><?php the_field('podnadpis_clanku')?></span>
                    </div>
                    <p class="blog--p">
                        <?php

                        $maxLength = 110;
                        $nazev = substr(get_field('prvni_odstavec'), 0, $maxLength);
                        echo "$nazev..";

                        ?>
                    </p>

                    <div class="blog--card__date">
                        <p><?php the_field('datum')?></p>
                    </div>

                    <div class="blog--card__signature">
                        <p><?php the_field('autor_clanku')?></p>
                    </div>
                </a>
            </div>
            <?php endwhile; ?>



    <?php

    $prev_link = get_previous_posts_link(__('Novější články'));
    $next_link = get_next_posts_link(__('Starší články'));
    // as suggested in comments
    if ($prev_link || $next_link) {
        echo '<div class="nav-butt">';
        if ($prev_link){
            echo '<button class="blog-button blog-button-prev blog-button-fix">';
            previous_posts_link('Novější články');
            echo '</button>';
        }
        if ($next_link){
            echo '<button class="blog-button blog-button-next blog-button-fix">';
            next_posts_link('Starší články');
            echo '</button>';
        }
        echo '</div>';
    }

    ?>

        </div>
    </div>
    


    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>





<?php get_footer(); ?>