
jQuery(document).ready(function($) {

    if($('#map').length){




// When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', initMap);

    function initMap() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 12,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(48.8553387,16.0462995), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [{"saturation": 36}, {"color": "#000000"}, {"lightness": 40}]
            }, {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [{"visibility": "on"}, {"color": "#000000"}, {"lightness": 16}]
            }, {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#000000"}, {"lightness": 20}]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#000000"}, {"lightness": 17}, {"weight": 1.2}]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{"color": "#000000"}, {"lightness": 20}]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{"color": "#000000"}, {"lightness": 21}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#000000"}, {"lightness": 17}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#000000"}, {"lightness": 29}, {"weight": 0.2}]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{"color": "#000000"}, {"lightness": 18}]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{"color": "#000000"}, {"lightness": 16}]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{"color": "#000000"}, {"lightness": 19}]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{"color": "#000000"}, {"lightness": 17}]
            }],

            zoomControl: true,
            gestureHandling: 'cooperative'
        };


        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(48.8553387,16.0462995),
            map: map,
            icon: '/wp-content/themes/peko/images/mobile-logo.png'
        });


        var contentString = '<div style="width:310px; font-family: \'montserrat\', sans-serif;line-height: 20px; font-size: 13px">' +
            '<div style="padding-left:12px;padding-top:12px;padding-bottom:12px;padding-right: 0px" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' +

            '<span><b>Peko studio</b></span><br>' +
            '<span>IČ: 05472792</span><br>' +
            '<span>Klácelova 57/3</span><br>' +
            '<span>Znojmo 66902</span><br>' +
            '</div>' +


            '<div style="padding-left:0px;padding-top:12px;padding-bottom:12px;padding-right: 12px"  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' +

            '<br>' +
            '<span><b>+420 731 171 595</b></span><br>' +
            '<span><a style="color: #e43b48;font-weight: bold;" href="mailto:info@peko-studio.cz">info@peko-studio.cz</a></span><br>' +
            '<br>' +

            '</div>' +

            '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });


        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });


    }
    }
});