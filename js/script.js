
function mapsSelector() {
    if((navigator.platform.indexOf("iPhone") != -1) ||
        (navigator.platform.indexOf("iPad") != -1) ||
        (navigator.platform.indexOf("iPod") != -1))
        //window.open("maps://maps.google.com/maps/dir//Kl%C3%A1celova+57%2F3,+Znojmo/@48.8553806,16.0457862,18.7z/data=!4m8!4m7!1m0!1m5!1m1!1s0x476d550862a64b8d:0xc205650776fc8f9e!2m2!1d16.0463857!2d48.8553555");
        window.open("https://www.google.com/maps/dir//Kl%C3%A1celova+57%2F3,+Znojmo/@48.8553806,16.0457862,18.7z/data=!4m8!4m7!1m0!1m5!1m1!1s0x476d550862a64b8d:0xc205650776fc8f9e!2m2!1d16.0463857!2d48.8553555");
        else /* else use Google */
            window.open("https://www.google.com/maps/dir//Kl%C3%A1celova+57%2F3,+Znojmo/@48.8553806,16.0457862,18.7z/data=!4m8!4m7!1m0!1m5!1m1!1s0x476d550862a64b8d:0xc205650776fc8f9e!2m2!1d16.0463857!2d48.8553555");
 }


var vid = document.getElementById("dev-video");


function playVid() {
    jQuery(document).ready(function($) {
        vid.play();
        $(".play-vid").css("display", "none");
        $(".video-dark").css("opacity", "0");
        $(".video-dark").css("transition", "0.2s");
        $("#dev-video").removeClass("dev-video-block");
    });

}

function pauseVid() {
    jQuery(document).ready(function($) {
    vid.pause();
    $(".play-vid").css("display","block");
    $(".video-dark").css("opacity","1");
    $(".pause-vid").css("display","none");
    $(".video-dark").css("transition","0.2s");
    $( "#dev-video" ).addClass( "dev-video-block" );
    });
}


function playVid2() {
    jQuery(document).ready(function($) {
    vid.play();
    $(".play-vid").css("display","none");
    $(".video-dark").css("opacity","0");
    $(".video-dark").css("transition","0.2s");
    $( "#dev-video" ).removeClass( "dev-video-block" );
    $(".pause-vid").css("display","block");
    $(".ref-video-text").css("display","none");

});
}

function pauseVid2() {
    jQuery(document).ready(function($) {
    vid.pause();
    $(".ref-video-text").css("display","block");
    $(".play-vid").css("display","block");
    $(".video-dark").css("opacity","1");
    $(".pause-vid").css("display","none");
    $(".video-dark").css("transition","0.2s");
    $( "#dev-video" ).addClass( "dev-video-block" );
});
}



var clicked = false;

function menuhover()
{
    if(clicked)
    {
        jQuery(document).ready(function($) {

        $(".header-menu-li").css("opacity","0");
        $(".header-menu-li").css("transition","opacity 0.2s");
        $(".menu").css("transition","0.2s");
        $(".menu").css("margin-top","55px");
        $(".menu").css("z-index","-2");
        $(".menu-arrow").css("opacity","0");
        $(".menu-icons").removeClass("change");
        $(".header-menu-ul a").css("display","none");
        $(".header-menu-sub-ul a").css("display","none");
        $(".menu-top-container a").css("pointer-events","none");
        $("#menu-top li").css("display","none");
        });
    }
    else
    {
        jQuery(document).ready(function($) {
        $(".header-menu-li").css("opacity","1");
        $(".header-menu-li").css("transition","opacity 0.2s");
        $(".menu").css("margin-top","70px");
        $(".menu").css("transition","0.2s");
        $(".menu").css("z-index","1");
        $(".menu-arrow").css("opacity","1");
        $(".menu-icons").addClass("change");
        $(".menu-top-container a").css("pointer-events","all");
        $(".header-menu-ul a").css("display","block");
        $(".header-menu-sub-ul a").css("display","block");
        $("#menu-top li").css("display","block");
        });
    }
    clicked = !clicked;
}




jQuery(function ($) {

    window.onscroll = function () {
        myFunction()
    };


    function myFunction() {
        if (window.pageYOffset > 1) {
            $('.fixed-menu').css('background', '#2d2d2d');
            $('.fixed-menu').css("transition", "0.5s");
        } else {
            $('.fixed-menu').css('background', 'transparent');
            $('.fixed-menu').css("transition", "0.5s");
        }
    }

});




jQuery(function ($) {
$('.responsive').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
            }
        }

        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});



$('.one-time').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
});




$('.slider-nav').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    dots: true,
    centerMode: true,
    focusOnSelect: true
});

});



jQuery(function ($) {
// Create a lightbox
    (function () {
        var $lightbox = $("<div class='lightbox'></div>");
        var $img = $("<img>");
        var $caption = $("<p class='caption'></p>");

        // Add image and caption to lightbox

        $lightbox
            .append($img)
            .append($caption);

        // Add lighbox to document

        $('body').append($lightbox);

        $('.hover-slick').click(function (e) {
            e.preventDefault();

            // Get image link and description
            var src = $('.lightbox-gallery img').attr("data-image-hd");
            var cap = $('.lightbox-gallery img').attr("alt");

            // Add data to lighbox

            $img.attr('src', src);
            $caption.text(cap);

            // Show lightbox

            $lightbox.fadeIn('fast');

            $lightbox.click(function () {
                $lightbox.fadeOut('fast');
            });
        });

    }());

});






jQuery(function ($) {

    $('.single-item').slick({
        infinite: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000
    });


    $('.single-item-rtl').slick({
        rtl: true
    });

});


jQuery(function ($) {
// Call & init
    $(document).ready(function () {
        $('.ba-slider').each(function () {
            var cur = $(this);
            // Adjust the slider
            var width = cur.width() + 'px';
            cur.find('.resize img').css('width', width);
            // Bind dragging events
            drags(cur.find('.handle'), cur.find('.resize'), cur);
        });
    });

// Update sliders on resize.
// Because we all do this: i.imgur.com/YkbaV.gif
    $(window).resize(function () {
        $('.ba-slider').each(function () {
            var cur = $(this);
            var width = cur.width() + 'px';
            cur.find('.resize img').css('width', width);
        });
    });

    function drags(dragElement, resizeElement, container) {

        // Initialize the dragging event on mousedown.
        dragElement.on('mousedown touchstart', function (e) {

            dragElement.addClass('draggable');
            resizeElement.addClass('resizable');

            // Check if it's a mouse or touch event and pass along the correct value
            var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

            // Get the initial position
            var dragWidth = dragElement.outerWidth(),
                posX = dragElement.offset().left + dragWidth - startX,
                containerOffset = container.offset().left,
                containerWidth = container.outerWidth();

            // Set limits
            minLeft = containerOffset + 10;
            maxLeft = containerOffset + containerWidth - dragWidth - 10;

            // Calculate the dragging distance on mousemove.
            dragElement.parents().on("mousemove touchmove", function (e) {

                // Check if it's a mouse or touch event and pass along the correct value
                var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

                leftValue = moveX + posX - dragWidth;

                // Prevent going off limits
                if (leftValue < minLeft) {
                    leftValue = minLeft;
                } else if (leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                // Translate the handle's left value to masked divs width.
                widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%';

                // Set the new values for the slider and the handle.
                // Bind mouseup events to stop dragging.
                $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
                    $(this).removeClass('draggable');
                    resizeElement.removeClass('resizable');
                });
                $('.resizable').css('width', widthValue);
            }).on('mouseup touchend touchcancel', function () {
                dragElement.removeClass('draggable');
                resizeElement.removeClass('resizable');
            });
            e.preventDefault();
        }).on('mouseup touchend touchcancel', function (e) {
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
    }


});

// AOS.init();

jQuery(function($){
    $(document).ready(function () {
            // load more
         function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        var queryRefsCount = getUrlVars()["query"];
        var max_posts_query = null;

        var setPN = queryRefsCount ? parseInt(queryRefsCount) / 9 : 1;
        
        var ppp = 9; // Post per page
        var cat = 8;
        var pageNumber = setPN;

function totalPosts(){
    $.ajax({
        type: "POST",
        url:my_ajax_object.ajax_url,
        data : {action: "total_posts_ajax"},
        success: function(data){
            // console.log("total count",data);
            max_posts_query = data;
        },
        error: function(er){
            console.log(er);
        }
    })
};
totalPosts();

function load_posts(){
    pageNumber++;
    ajax_url_scroll = "<?php echo admin_url('admin-ajax.php'); ?>";
    var str = '&cat=' + cat + '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_post_ajax';
    $.ajax({
        type: "POST",
        dataType: "html",
        url: my_ajax_object.ajax_url,  //ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $data = $(data);
            if($data.length){
                $(".ref-button-wrap").before($data);
                // console.log($data);
            } 
        },
        error : function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        }

    });
    return false;
}

    var currentQuery = !queryRefsCount ? 9 : parseInt(queryRefsCount);
    // console.log(currentQuery);
    $("#more_refs").click(function(e){
        //e.preventDefault();
        //var hasQueryParam = new URLSearchParams(window.location.href);
        currentQuery += 9;
        var pageUrl = '?' +"query="+currentQuery;
        window.history.pushState('', '', pageUrl);
        load_posts();
        if(max_posts_query <= currentQuery){
            $("#more_refs").remove();
        }      
    });

    // platforms
    var platform_url = window.location.href;
    var getPlatformSlug= platform_url.split("/");
    var platformSlug;

    // if(getPlatformSlug[4] == "ios" || getPlatformSlug[4] == "webove-aplikace" || getPlatformSlug[4] == "android"){
    //     platformSlug = getPlatformSlug[4];
    //     console.log("if", platformSlug);
    //     return;
    // }
    // console.log("after if", platformSlug);
    function getPlatformSlugName(){
        if(getPlatformSlug[4] == "ios" || getPlatformSlug[4] == "android" || getPlatformSlug[4] == "webove-aplikace"){
            console.log("current slug", getPlatformSlug[4]);
            return getPlatformSlug[4];
        }
    }


var max_platform_posts = null;
function platformTotalPosts(){
    $.ajax({
        type: "POST",
        url:my_ajax_object.ajax_url,
        data : {
            action: "platform_total_posts_ajax",
            platformType: getPlatformSlugName(),
            pageNumber: pageNumber
        },
        success: function(data){
            console.log("total count slug",data);
            max_platform_posts = data;
            if(parseInt(data) == 9){
                $("#more_refs_platform").remove();
            }
        },
        error: function(er){
            console.log(er);
        }
    })
};
    platformTotalPosts();

// hide load more btn

(function hide_platform_button(){
    //$("#more_refs_platform").remove();
})()



    function load_platforms_refs(type){
        pageNumber++;
        ajax_url_scroll = "<?php echo admin_url('admin-ajax.php'); ?>";
        var str = '&cat=' + cat + '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&platformType=' + type + '&action=more_post_platform';
        $.ajax({
            type: "POST",
            dataType: "html",
            url: my_ajax_object.ajax_url,  //ajax_posts.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);
                if($data.length){
                    $(".ref-button-wrap").before($data);
                    console.log($data);
                } 
            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
    
        });
        return false;
       }

    $("#more_refs_platform").click(function(e){
        console.log("klik");
        currentQuery += 9;
        var pageUrl = '?' +"query="+currentQuery;
        window.history.pushState('', '', pageUrl);
        var currentViewSlug = getPlatformSlugName();
        console.log(max_platform_posts);
        load_platforms_refs(currentViewSlug);
        if(max_platform_posts <= currentQuery){
            $("#more_refs_platform").remove();
        }

    })


    });

})

