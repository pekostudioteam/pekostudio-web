module.exports = function (grunt) {

	require('jit-grunt')(grunt);

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'css/style.css': 'src/scss/style.scss'
                }
            }
        },

        concat: {
            dist: {
                src: [
                    // JS files inicialization
                    /*'src/js/library/select2.full.min.js',
                    'src/js/library/jquery.slicknav.js',
                    'src/js/library/jquery.responsiveTabs.min.js',
                    'src/js/library/vegas.min.js',
                    'src/js/gmap.js',*/
                    'src/js/script.js'
                ],
                dest: 'js/script.js'
            }
	  	},

		autoprefixer: {
            options: {
                    map: {
                        prev: 'css/',
                        inline: false
                	},
            	browsers: ['last 3 version', 'ie 9']
            },
                style: {
                src: 'css/style.css',
            	dest: 'css/style.css'
            }
        },
		
		pixrem: {
			options: {
				rootvalue: '10px',
			},
			style: {
				src: 'css/style.css',
				dest: 'css/style.css'
			}
		},



		cssmin: {
  			target: {
    			files: [{
      				expand: true,
      				cwd: 'css/',
      				src: ['*.css', '!*.min.css'],
      				dest: 'css/',
      				ext: '.min.css'
    			}]
  			}
		},


        cssnano: {
            options: {
                sourcemap: false
            },
            dist: {
                files: {
                    'css/style.css': 'css/style.css'
                }
            }
        },

		watch: {
			sass: {
				files: ['src/scss/**/*.*'],
				tasks: ['doCSS'],
				options: {
				    livereload: {
				    	host: 'localhost',
        				port: 9001,
				    },
				},
			},
			js: {
				files: ['src/js/*.js'],
				tasks: ['doJS'],
				options: {
				    livereload: {
				    	host: 'localhost',
        				port: 9001,
				    },
				},
			},
			html: {
				files: ['../*.html'],
				options: {
					livereload: {
				    	host: 'localhost',
        				port: 9001,
				    },
				},
			},
			images: {
				files: ['../img/sprites/*.*'],
				tasks: ['sprite', 'buildcss:style']
			},
		},

		hashres: {
			options: {
				encoding: 'utf8',
				fileNameFormat: '${name}.${ext}?${hash}',
				renameFiles: false
			},
			dist: {
				src: [
					'../images/*.{png,jpg,gif}',
					'../images/*/*.{png,jpg,gif}',
				],
				dest: [
					'../css/style.css',
					'*.php',
				],
			}
		}
	});

    grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');


	grunt.registerTask('doCSS',  ['sass', 'pixrem']);
	grunt.registerTask('doJS',  ['concat']);
	grunt.registerTask('doBASIC', ['doCSS', 'doJS', 'sprite', 'cssnano']);
	

	grunt.registerTask('default', ['concat', 'sass', 'pixrem']);
	grunt.registerTask('production', ['concat', 'sass', 'hashres']);

}