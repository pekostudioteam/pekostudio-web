<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php
    if (get_field('align4', 'options')=="left"){
        $align="left";
    }
    else{
        $align="right";
    }
    ?>
    <header class="secondary-header single-reference ref-bg"   style="background: url(<?php the_field('ref_head_photo', 'options') ?>) bottom <?php echo $align; ?>; background-size: cover">
        <div class="header-shaddow">

            <div class="row content">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>

            <div class="row content">
                <h1>Reference</h1>
            </div>

        </div>
    </header>




    <div class="ref-detail-heading">
        <div class="container">
            <div class="platform platform__reference">


                <?php if(get_field('android_app')){ ?>
                    <span class="platform-title"><?php get_template_part('svg/ico', 'android') ?>&nbsp;Android</span>
                <?php } ?>
                <?php if(get_field('ios_app')){ ?>
                    <span class="platform-title"><?php get_template_part('svg/ico', 'apple') ?>&nbsp;iOS</span>
                <?php } ?>
                <?php if(get_field('smartwatch_app')){ ?>
                    <span class="platform-title"><?php get_template_part('svg/ico', 'watch') ?>&nbsp;Smartwatch</span>
                <?php } ?>
                <?php if(get_field('web_app')){ ?>
                    <span class="platform-title"><?php get_template_part('svg/ico', 'desktop') ?>&nbsp;Web</span>
                <?php } ?>

            </div>
            <div class="row margintop70 ref-desktop-images">



                <?php if(get_field('desktop_mobile_video')=="mobil"){ ?>
                    <div class="col-sm-4">
                        <img class="ref-detail-heading-img" src="<?php the_field('fotografie_leva') ?>">
                    </div>
                    <div class="col-sm-4">
                        <img class="ref-detail-heading-img" src="<?php the_field('ilustracni_fotografie') ?>">
                    </div>
                    <div class="col-sm-4">
                        <img class="ref-detail-heading-img" src="<?php the_field('fotografie_prava') ?>">
                    </div>
                <?php }?>

                <?php if(get_field('desktop_mobile_video')=="desktop"){ ?>
                    <div class="col-sm-4">
                        <img class="ref-detail-heading-img ref-detail-heading-img--desktop" src="<?php the_field('fotografie_leva') ?>">
                    </div>
                    <div class="col-sm-4">
                        <img class="ref-detail-heading-img ref-detail-heading-img--desktop" src="<?php the_field('ilustracni_fotografie') ?>">
                    </div>
                    <div class="col-sm-4">
                        <img class="ref-detail-heading-img ref-detail-heading-img--desktop" src="<?php the_field('fotografie_prava') ?>">
                    </div>
                <?php }?>




            </div>
            <div class="ref-phone-images">
                <div class="one-time">





                    <div><img class="slick-img" src="<?php the_field('ilustracni_fotografie') ?>"></div>

                    <?php if(get_field('fotografie_leva')){?>
                        <div><img class="slick-img" src="<?php the_field('fotografie_leva') ?>"></div>
                    <?php }?>
                    <?php if(get_field('fotografie_prava')){?>
                        <div><img class="slick-img" src="<?php the_field('fotografie_prava') ?>"></div>
                    <?php }?>



                </div>
            </div>
        </div>
    </div>

    <div class="container subpage reference-detail-page">
    <div class="row">
        <?php if(get_field('video')){ ?>
            <div class="col-md-4 col-sm-4"></div>
            <div class="col-md-8 col-sm-8"><h2><?php the_field('nadpis_reference') ?></h2></div>
        <?php }  else { ?>
            <div class="col-xs-12"><h2 style="text-align: center"><?php the_field('nadpis_reference') ?></h2></div>

        <?php }  ?>
    </div>

    <div class="row">
    <?php if(get_field('video')){ ?>
        <?php if(get_field('desktop_mobile_video')=="mobil"){ ?>

            <?php if(get_field('apple_store_url')||get_field('google_play_url')||get_field('web_url')){ ?>

                <?php if(get_field('android_app') && get_field('ios_app')){?>
                    <div class="col-md-4 col-sm-4 col-xs-6 mobile-size">
                <?php } else { ?>
                    <div class="col-md-4 col-sm-4 col-xs-6 mobile-size android-size">
                <?php }  ?>

            <?php }else{  ?>

                <?php if(get_field('android_app') && get_field('ios_app')){?>
                    <div class="col-md-4 col-sm-4 col-xs-6 mobile-size mobile-size__nobtn">
                <?php } else { ?>
                    <div class="col-md-4 col-sm-4 col-xs-6 mobile-size mobile-size__nobtn android-size">
                <?php }  ?>
            <?php }  ?>


            <div class="ref-video-wrap">

                <div class="dev-video-wrap">
                    <div class="video-dark video-dark__ref"></div>
                    <video id="dev-video"  class="dev-video dev-video__ref dev-video-block" poster="<?php the_field('screen_shot') ?>">
                        <source src="<?php the_field('video') ?>" type="video/mp4">
                        <source src="<?php the_field('video') ?>" type="video/webm">
                    </video>
                    <div class="ref-video-play pause-vid pause-vid__ref" onclick="pauseVid2()">
                        <div class="pause-vid-butt pause-vid-butt__ref"></div>

                    </div>
                    <div class="ref-video-play play-vid play-vid__ref" onclick="playVid2()">
                        <div class="play-vid-butt play-vid-butt__ref"></div>
                    </div>
                </div>



                <?php  if(get_field('android_app')){?>

                        <?php if(get_field('android_app') && get_field('ios_app')){?>
                            <img class="ref-video-img ref-video-img-2" src="<?php echo get_stylesheet_directory_uri() ?>/images/iphone.png">
                        <?php } else { ?>
                            <img class="ref-video-img ref-video-img-2 ref-video-img-android" src="<?php echo get_stylesheet_directory_uri() ?>/images/android-phone.png">
                        <?php }  ?>


                    <?php } else { ?>
                        <img class="ref-video-img ref-video-img-2" src="<?php echo get_stylesheet_directory_uri() ?>/images/iphone.png">
                    <?php }  ?>




                <div class="ref-video-text">
                    <span>Přehrát<br>video</span>
                </div>


            </div>
            </div>

        <?php }  ?>




        <?php if(get_field('desktop_mobile_video')=="desktop"){ ?>
            <?php if(get_field('apple_store_url')||get_field('google_play_url')||get_field('web_url')){ ?>
                <div class="col-md-4 col-sm-4 col-xs-6 desktop-size">
            <?php }else{  ?>
                <div class="col-md-4 col-sm-4 col-xs-12 desktop-size desktop-size__nobtn">
            <?php }  ?>

            <div class="ref-video-wrap">

                <div class="dev-video-wrap dev-video-wrap__desktop">
                    <div class="video-dark video-dark__ref video-dark__ref--desktop"></div>
                    <video id="dev-video"  class="dev-video dev-video__ref dev-video__ref--desktop dev-video-block" poster="<?php the_field('screen_shot') ?>">
                        <source src="<?php the_field('video') ?>" type="video/mp4">
                        <source src="<?php the_field('video') ?>" type="video/webm">
                    </video>
                    <div class="ref-video-play pause-vid pause-vid__ref ref-video-play--desktop" onclick="pauseVid2()">
                        <div class="pause-vid-butt pause-vid-butt__ref"></div>

                    </div>
                    <div class="ref-video-play play-vid play-vid__ref ref-video-play--desktop" onclick="playVid2()">
                        <div class="play-vid-butt play-vid-butt__ref"></div>
                    </div>
                </div>


                <img class="ref-video-img ref-video-img-2 ref-video-img-2--desktop" src="<?php echo get_stylesheet_directory_uri() ?>/images/imac.png">

                <div class="ref-video-text ref-video-text--desktop">
                    <span>Přehrát<br>video</span>
                </div>


            </div>
            </div>

        <?php }  ?>
    <?php }  ?>


    <?php if(get_field('video')){ ?>
        <?php if(get_field('apple_store_url')||get_field('google_play_url')||get_field('web_url')){ ?>

            <div class="col-xs-6 ref-buttons-phone">

        <?php }else{  ?>
            <div>
        <?php }  ?>


    <?php } else { ?>
        <div class="col-xs-12 ref-buttons-phone  ref-buttons-align ref-buttons-novideo">
    <?php } ?>

    <div class="ref-buttons-phone-wrap">

        <?php if(get_field('google_play_url')){ ?>
            <a target="_blank" href="<?php the_field('google_play_url') ?>"><button class="ref-button-google">Google Play</button></a><br>
        <?php } ?>

        <?php if(get_field('apple_store_url')){ ?>
            <a target="_blank" href="<?php the_field('apple_store_url') ?>"><button class="ref-button-apple">Apple Store</button></a><br>
        <?php } ?>

        <?php if(get_field('web_url')){ ?>
            <a target="_blank" href="<?php the_field('web_url') ?>"><button class="ref-button-web">Web</button></a>
        <?php } ?>

    </div>
    </div>

    <?php if(get_field('video')){ ?>
    <div class="col-md-8 col-sm-8 col-xs-12">

    <?php } else { ?>
    <div class="col-md-12 col-sm-12 col-xs-12  ref-buttons-align">
    <?php } ?>


    <p><?php the_field('prvni_odstavec')?></p>

    <p><?php the_field('druhy_odstavec')?></p>

    <p><?php the_field('treti_odstavec')?></p>
    <div class="ref-buttons-desktop">
        <?php if(get_field('google_play_url')){ ?>
            <a target="_blank" href="<?php the_field('google_play_url') ?>"><button class="ref-button-google">Google Play</button></a>
        <?php } ?>

        <?php if(get_field('apple_store_url')){ ?>
            <a target="_blank" href="<?php the_field('apple_store_url') ?>"><button class="ref-button-apple">Apple Store</button></a>
        <?php } ?>

        <?php if(get_field('web_url')){ ?>
            <a target="_blank" href="<?php the_field('web_url') ?>"><button class="ref-button-web">Web</button></a>
        <?php } ?>
    </div>
    </div>
    </div>
    </div>




    <?php if(get_field('fotogalerie')){ ?>

        <div class="responsive reference_fotogalerie">


            <?php while ( have_rows('fotogalerie') ) : the_row(); ?>

                <div>
                    <div class="lightbox-gallery">
                        <div>
                            <div class="hover-slick"></div>
                            <img class="slick-img" data-image-hd="<?php the_sub_field('fotky_gallery')?>" src="<?php the_sub_field('fotky_gallery')?>">
                        </div>
                    </div>
                </div>


            <?php endwhile; ?>


        </div>
    <?php } ?>




    <div class="what-next__wrap-other">
        <?php get_template_part('parts/category', 'what-next') ?>

    </div>



    <footer class="secondary-footer">
        <?php get_template_part('parts/category', 'short-contact') ?>
    </footer>



<?php endwhile; ?>
<?php get_footer(); ?>