<?php get_header(); ?>
    <!-- Hlavička - začátek -->
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <header class="header">
        <div class="row-header">
            <div class="header-shaddow">
                <div class="header-video">
                    <video loop muted autoplay poster="/wp-content/themes/peko/images/ps.jpg" class="header-video-tag">
                        <source src="/wp-content/themes/peko/images/ps.mp4" type="video/mp4">
                        <source src="/wp-content/themes/peko/images/pswebm.webm" type="video/webm">

                    </video>
                </div>

                <div class="row content">

                    <?php get_template_part('parts/category', 'menu') ?>


                    <div class="header-text">

                        <div class="header-text-sentences">
                            <div class="hexagon-animated">
                                <?php get_template_part('svg/ico', 'mnohouhelnik') ?>
                            </div>
                            <h1 class="h1__polygon"><?php the_field('hlavni_nadpis')?></h1>
                            <p><?php the_field('text_pod_nadpisem_1')?><br class="enter-sm-smazat"> <?php the_field('text_pod_nadpisem_2')?></p>
                            <div class="header-button-wrap">
                                <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-contact.php' ) ) ?>"><button class="hp--header-kontakt">Kontakt</button></a>
                                <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-reference.php' ) ) ?>"><button class="hp--header-reference">Reference</button></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Hlavička - konec -->

    <!-- První blok - začátek -->


    <div class="container nopadding">

        <img alt="logo-shadow" title="" src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-stin.jpg" class="container--logo">

    </div>

    <div class="container container--first paddingbottom10">
        <div class="row">
            <div class="apps-info col-lg-12">
                <h2><?php the_field('nadpis_prvni')?> <br class="enter-inside-text"><?php the_field('nadpis_prvni_2')?></h2>
                <p><?php the_field('text_prvni')?></p>
            </div>
        </div>
    </div>
    <div class="container nopadding">
        <div class="block-wrap margintop-60">
            <a href="tvorba-android-aplikaci">
                <div class="block block--android">

                    <div class="hp--small-hexagon hp--android">
                        <?php get_template_part('svg/ico', 'android') ?>
                    </div>

                    <h3 class="block--text">Tvorba Android aplikací</h3>
                </div>
            </a>

            <a href="tvorba-webovych-aplikaci">
                <div class="block block--apps">
                    <div class="hp--small-hexagon hp--apps">
                        <?php get_template_part('svg/ico', 'web_apps') ?>
                    </div>
                    <h3 class="block--text">Tvorba webových aplikací</h3>
                </div>
            </a>

            <a href="tvorba-ios-aplikaci">
                <div class="block block--apple">
                    <div class="hp--small-hexagon hp--apple">
                        <?php get_template_part('svg/ico', 'apple') ?>
                    </div>
                    <h3 class="block--text">Tvorba iOS aplikací</h3>
                </div>
            </a>

            <a href="tvorba-webovych-stranek">
                <div class="block block--html">
                    <div class="hp--small-hexagon hp--html">
                        <?php get_template_part('svg/ico', 'web_page') ?>
                    </div>
                    <h3 class="block--text">Tvorba webových stránek</h3>
                </div>
            </a>

            <img alt="logo-shadow2" title="" src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-stin2.jpg" class="container--logo2">
        </div>

    </div>

    <!-- První blok - konec -->

    <!-- Druhý blok - začátek -->
    <div class="mobile-apps">

        <div class="polygon1">
        </div>

        <div class="mobile-apps--wrap">
            <div class="mobile-apps-background"></div>
            <div class="container">
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="mobile-apps--text">
                            <div class="platform">

                                <?php if(get_field('android_apps_druhy')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'android') ?>&nbsp;Android</span>
                                <?php } ?>
                                <?php if(get_field('ios_apps_druhy')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'apple') ?>&nbsp;iOS</span>
                                <?php } ?>
                                <?php if(get_field('smartwatch_apps_druhy')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'watch') ?>&nbsp;Smartwatch</span>
                                <?php } ?>
                                <?php if(get_field('web_apps_druhy')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'desktop') ?>&nbsp;Web</span>
                                <?php } ?>

                            </div>
                        </div>
                        <div class="hp-photo-ref hp-photo-ref-left">

                            <!-- <img alt="reference-img" title="" class="hp-photo-ref-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/first-reference.png"> -->
                            <img alt="reference-img" title="" class="hp-photo-ref-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/hp1-min.png">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mobile-games--margin-top">
                        <h2><?php the_field('nadpis_druhy')?> <br><?php the_field('nadpis_druhy_2')?></h2>
                        <p><?php the_field('text_druhy')?></p>
                        <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-reference.php' ) ) ?>"><button class="hp--mobile-apps-button">Více referencí</button></a>
                    </div>

                </div>
            </div>
        </div>

        <div class="polygon2">
        </div>

    </div>
    <!-- Druhý blok - konec -->




    <!-- Třetí blok - začátek -->
    <div class="mobile-games">

        <div class="polygon3">
        </div>

        <div class="mobile-games--wrap">
            <div class="container container--mobile-games">
                <div class="row">



                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">
                        <div class="mobile-games--icons">
                            <div class="platform dark-svg">

                                <?php if(get_field('android_apps_treti')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'android') ?>&nbsp;Android</span>
                                <?php } ?>
                                <?php if(get_field('ios_apps_treti')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'apple') ?>&nbsp;iOS</span>
                                <?php } ?>
                                <?php if(get_field('smartwatch_apps_treti')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'watch') ?>&nbsp;Smartwatch</span>
                                <?php } ?>
                                <?php if(get_field('web_apps_treti')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'desktop') ?>&nbsp;Web</span>
                                <?php } ?>

                            </div>
                        </div>
                        <div class="hp-photo-ref hp-photo-ref-right">
                            <!-- <img alt="reference-img" title="" class="hp-photo-ref-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/second-reference.png"> -->
                            <img alt="reference-img" title="" class="hp-photo-ref-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/hp2-min.png">
                        </div>
                    </div>

                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6 col-xs-12 mobile-apps--margin-top">
                        <h2><?php the_field('nadpis_treti')?> <br><?php the_field('nadpis_treti_2')?></h2>
                        <p><?php the_field('text_treti')?></p>
                        <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-reference.php' ) ) ?>"><button class="hp--mobile-games-button">Více referencí</button></a>
                    </div>



                </div>
            </div>
        </div>

        <div class="polygon4">
        </div>

    </div>
    <!-- Třetí blok - konec -->





    <!-- Čtvrtý blok - začátek -->
    <div class="web-apps">

        <div class="polygon5">
        </div>

        <div class="web-apps--wrap">
            <div class="web-apps-background"></div>
            <div class="container web-apps--container">
                <div class="row">

                    <div class="col-lg-6 col-md- col-sm-6">
                        <div class="web-apps--icon-wrap">
                            <div class="platform web-svg">

                                <?php if(get_field('android_apps_ctvrty')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'android') ?>&nbsp;Android</span>
                                <?php } ?>
                                <?php if(get_field('ios_apps_ctvrty')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'apple') ?>&nbsp;iOS</span>
                                <?php } ?>
                                <?php if(get_field('smartwatch_apps_ctvrty')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'watch') ?>&nbsp;Smartwatch</span>
                                <?php } ?>
                                <?php if(get_field('web_apps_ctvrty')){ ?>
                                    <span class="platform-title"><?php get_template_part('svg/ico', 'desktop') ?>&nbsp;Web</span>
                                <?php } ?>

                            </div>
                        </div>
                        <div class="hp-photo-ref hp-photo-ref-left">
                            <!-- <img alt="reference-img" title="" class="hp-photo-ref-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/third-reference.png"> -->
                            <img alt="reference-img" title="" class="hp-photo-ref-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/hp3-min.png">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 web-apps--margin-top">
                        <h2><?php the_field('nadpis_ctvrty')?> <br><?php the_field('nadpis_ctvrty_2')?></h2>
                        <p><?php the_field('text_ctvrty')?></p>
                        <!-- <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-reference.php' ) ) ?>"><button class="hp--web-apps-button">Více referencí</button></a> -->
                        <a href="/platforma/webove-aplikace/"><button class="hp--web-apps-button">Více referencí</button></a>
                    </div>

                </div>
            </div>
        </div>

        <div class="polygon6">
        </div>

    </div>
    <!-- Čtvrtý blok - konec -->




    <!-- Pátý blok - začátek -->
    <div class="team hp__team">

        <div class="container">
            <div class="row">
                <div class="team--text">
                    <div class="col-lg-12 col-md-12">
                        <h2><?php the_field('nadpis_kdo_1', 'options')?> <br><?php the_field('nadpis_kdo_2', 'options')?></h2>
                        <p>
                            <?php the_field('prvni_veta_kdo', 'options')?>
                            <br>
                            <?php the_field('druha_veta_kdo', 'options')?>
                            <br>
                            <?php the_field('treti_veta_kdo', 'options')?>
                        </p>
                    </div>
                </div>

                <!-- MOBIL -->

                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto2.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_prvni', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_prvni', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_prvni', 'options')?></span>
                        </div>
                    </div>
                </div>

                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto1.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_druhy', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_druhy', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_druhy', 'options')?></span>
                        </div>
                    </div>
                </div>
                <div class="team--more--mobile__wrap">

                    <div class="team--more--mobile__info1">
                        <div class="team--more--mobile__photo">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/foto3.png">
                        </div>
                        <div class="team--more--mobile__text">
                            <b class="team--text--mobile__name"><?php the_field('jmeno_treti', 'options')?></b><br>
                            <span class="team--text--mobile__position"><?php the_field('funkce_treti', 'options')?></span><br>
                            <span class="team--text--mobile__other"><?php the_field('doplnek_treti', 'options')?></span>
                        </div>
                    </div>
                </div>



                <!-- TABLET + DESKTOP -->
                <div class="team--photo">
                </div>


                <div class="team--more__wrap">
                    <span class="more more--first"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--first__text">


                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_prvni', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_prvni', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_prvni', 'options')?></span>
                        </div>


                        <div class="team--text__block">
                        </div>
                    </div>
                </div>



                <div class="team--more__wrap2">
                    <span class="more more--second"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--second__text">
                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_druhy', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_druhy', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_druhy', 'options')?></span>
                        </div>
                        <div class="team--text__block">
                        </div>
                    </div>
                </div>


                <div class="team--more__wrap3">
                    <span class="more more--third"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/plus.png">
                    </span>
                    <div class="more--third__text">
                        <div class="div-name">
                            <b class="team--text__name"><?php the_field('jmeno_treti', 'options')?></b>
                        </div>
                        <div class="div-position">
                            <span class="team--text__position"><?php the_field('funkce_treti', 'options')?></span><br>
                        </div>
                        <div class="div-other">
                            <span class="team--text__other"><?php the_field('doplnek_treti', 'options')?></span>
                        </div>
                        <div class="team--text__block">
                        </div>
                    </div>
                </div>







                <div class="review--wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="single-item">
                    <?php while ( have_rows('recenze') ) : the_row(); ?>
                        <div>
                    <div class="row">
                                <div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-9 col-sm-push-3 col-xs-12 ">
                                    <div class="review--text">
                                        <p class="review--p">
                                            „<?php the_sub_field('text_recenze') ?>“
                                        </p>

                                        <div class="review--signature"><span class="review--text__signature"><b><?php the_sub_field('nazev_spolecnosti_recenze') ?></b><br class="enter-mobile"> <a target="_blank" href="http://<?php the_sub_field('odkaz_recenze') ?>"><?php the_sub_field('odkaz_recenze') ?></a></span></div>

                                    </div>
                                </div>

                                <div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-3 col-sm-pull-9 col-xs-12 text-right">
                                    <div class="review--photo">
                                        <img alt="quotation-mark" title="" class="quotation-mark" src="<?php echo get_stylesheet_directory_uri() ?>/images/uvozovky.png">
                                        <div class="review--photo__wrap">
                                                <img alt="human-photo" title="" class="review--photo__man" src="<?php the_sub_field('foto_recenze') ?>">
                                        </div>
                                    </div>
                                </div>
                    </div>
                        </div>

                    <?php endwhile; ?>

                    </div>
                </div>









            </div>
        </div>
    </div>

    <!-- Pátý blok - konec -->



<div class="cooperations-partners">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Mezi naše klienty patří například</h3>
                <div class="clients-wrap">
                    <div class="container-shorter dev-logos">
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/tmobile.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/ct.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/radiocas.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/erabanq.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/dogtrace.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/1.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/ismlouva.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/coop/karlovauni.png"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <!-- Šestý blok - začátek -->
    <div class="why-us">
        <div class="why-us--white-block"></div>

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12 why-us--gray-background">
                    <h3>Proč my?</h3>

                    <div>
                        <span class="check-svg"><?php get_template_part('svg/ico', 'check') ?></span>
                        <h4><?php the_field('nadpis_procmy_1')?></h4>
                    </div>
                    <p><?php the_field('odstavec_procmy_1')?></p>

                    <div>
                        <span class="check-svg"><?php get_template_part('svg/ico', 'check') ?></span>
                        <h4><?php the_field('nadpis_procmy_2')?></h4>
                    </div>
                    <p><?php the_field('odstavec_procmy_2')?></p>

                    <div>
                        <span class="check-svg"><?php get_template_part('svg/ico', 'check') ?></span>
                        <h4><?php the_field('nadpis_procmy_3')?></h4>
                    </div>
                    <p><?php the_field('odstavec_procmy_3')?></p>

                </div>

                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 why-us--white-background">
                    <h3>Technologie</h3>
                    <p><?php the_field('prvni_odstavec_tech','options')?></p>

                    <p><?php the_field('druhy_odstavec_tech','options')?></p>

                    <p><?php the_field('treti_odstavec_tech','options')?></p>
                    <div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/1.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/3.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/5.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/kotlin.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/node.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/2.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/vuejs.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/13.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/15.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/16.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/amazon.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/14.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/12.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/4.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/6.png"></div>
                        <div class="partners-logo-wrap"><img class="partners-logo" src="<?php echo get_stylesheet_directory_uri() ?>/images/partners/10.png"></div>





                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Šestý blok - konec -->

    <div class="blog">
        <div class="container">

            <div class="row child-hp-blog">
                <h3>Blog</h3>



                <?php

                $args = array(
                    'post_type'=> 'post',
                    'orderby'    => 'ID',
                    'post_status' => 'publish',
                    'order'    => 'DESC',
                    'posts_per_page' => 3 // this will retrive all the post that is published
                );
                $result = new WP_Query( $args );
                if ( $result-> have_posts() ) : ?>
                    <?php while ( $result->have_posts() ) : $result->the_post(); ?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12  blog--card">
                            <a href="<?php the_permalink() ?>">
                                <div class="blog-img-wrap">
                                    <img alt="jak-urychlit-programovani-v-PHP-photo" title="" class="blog--card__photo" src="<?php the_field('fotografie')?>">
                                </div>
                                <div class="blog-title-wrap">
                                    <span class="article-title">
                                        <?php the_field('nadpis_clanku')?>
                                        <br>
                                        <?php the_field('podnadpis_clanku')?>
                                    </span>
                                </div>



                                <p class="blog--p">
                                    <?php

                                    $maxLength = 110;
                                    $nazev = substr(get_field('prvni_odstavec'), 0, $maxLength);
                                    echo "$nazev..";

                                    ?>
                                </p>

                                <div class="blog--card__date">
                                    <p><?php the_field('datum')?></p>
                                </div>

                                <div class="blog--card__signature">
                                    <p><?php the_field('autor_clanku')?></p>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>


                <div class="col-xs-12">
                    <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-blog.php' ) ) ?>"><button class="hp--blog-button">Více článků</button></a>
                </div>

            </div>
        </div>
    </div>








    <!-- Patička - začátek -->

    <footer class="footer" id="mail">

        <div class="container">
            <div class="row">
                <h3>Kontakt</h3>

                <div class="col-lg-5 col-md-12">
                    <h4>Co dále?</h4>
                    <p><?php the_field('prvni_odstavec', 'options') ?></p>
                    <p><?php the_field('druhy_odstavec', 'options') ?></p>

                    <?php get_template_part('parts/function', 'mail') ?>
                    <div class="contact-form contact-form-hp">
                        <?php echo do_shortcode("[contact-form-7 id=\"375\" title=\"Kontaktní formulář 1\"]") ?>
                        <p class="gdpr-p hp-gdpr-p"><?php the_field('gdpr_veta', 'options') ?></p>
                    </div>
                </div>

                <div class="col-lg-7 col-md-12 contact--info">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 main-contact-info">
                            <a href="tel:<?php the_field('telefon', 'options') ?>"><span class="contact_svg contact_svg--tel"><?php get_template_part('svg/ico', 'tel') ?></span><span class="footer--phone"><?php the_field('telefon', 'options') ?></span></a>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 main-contact-info">
                            <a href="mailto:<?php the_field('email', 'options') ?>"><span class="contact_svg contact_svg--mail"><?php get_template_part('svg/ico', 'mail') ?></span><span class="footer--email"><?php the_field('email', 'options') ?></span></a>
                        </div>

                        <div class="col-lg-12">

                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div id="map"></div>



    </footer>
<?php endwhile; ?>
    <!-- Patička - konec -->
<div class="footer-front-page">
<?php get_footer(); ?>
</div>
